package be.samclercky.httpclient.exampleactions;

import be.samclercky.httpclient.HttpEventListener;
import be.samclercky.httpclient.actions.Action;
import be.samclercky.httpclient.actions.ActionTypes;
import be.samclercky.httpclient.exampleactions.respons.AddRespons;
import com.google.gson.Gson;

public class AddAction implements Action {
    private HttpEventListener listener;
    private Score data;

    public AddAction(HttpEventListener listener, Score data) {
        this.listener = listener;
        this.data = data;
    }

    @Override
    public String getURL() {
        return "https://heropath-a44b8.firebaseio.com/highscores.json";
    }

    @Override
    public ActionTypes getActionType() {
        return ActionTypes.POST;
    }

    @Override
    public String getRawDataToBeSend() {
        return (new Gson()).toJson(data);
    }

    @Override
    public HttpEventListener getListener() {
        return listener;
    }

    @Override
    public AddRespons getDataFromRequestedJSON(String json) {
        return (new Gson()).fromJson(json, AddRespons.class);
    }
}
