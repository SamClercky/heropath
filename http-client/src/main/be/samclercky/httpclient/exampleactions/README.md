# Example Actions

The library contains 2 example actions. One for retrieving data from
Google Firebase and one which alters data.

Every action must implement the Action interface and specifies one
of the ActionTypes.

Data will be parsed and returned with a repons that needs to be defined
by the user.

These actions are used in the game HeroPath.

## REST-api

### Get all data
GET https://heropath-a44b8.firebaseio.com/highscores.json

***respons:***
```
[
    null,
    {
        "name": "SampleName1",
        "score": 2300
    },
    {
        "name": "SampleName2",
        "score": 2200
    },
    {
        "name": "SampleName3",
        "score": 200
    }
]
```

### Add new record
POST https://heropath-a44b8.firebaseio.com/highscores.json

***data to be send:***
```
{
    "name": "My favorite hero",
    "score": 20000
}
```

***respons:***
```
{
    "name": "-M3wCRQEJxZ32KAIgXBd" # The generated id
}
```