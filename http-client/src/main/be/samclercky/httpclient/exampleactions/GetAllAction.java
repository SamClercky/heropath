package be.samclercky.httpclient.exampleactions;

import be.samclercky.httpclient.HttpEventListener;
import be.samclercky.httpclient.actions.Action;
import be.samclercky.httpclient.actions.ActionTypes;
import be.samclercky.httpclient.exampleactions.respons.GetAllRespons;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class GetAllAction implements Action {
    private HttpEventListener listener;

    public GetAllAction(HttpEventListener listener) {
        this.listener = listener;
    }

    @Override
    public String getURL() {
        return "https://heropath-a44b8.firebaseio.com/highscores.json";
    }

    @Override
    public ActionTypes getActionType() {
        return ActionTypes.GET;
    }

    /**
     * No JSON body to send
     * @return null because there is no body
     */
    @Override
    public String getRawDataToBeSend() {
        return null;
    }

    @Override
    public HttpEventListener getListener() {
        return listener;
    }

    @Override
    public GetAllRespons getDataFromRequestedJSON(String json) {
        Type type = new TypeToken<Map<String, Score>>() {}.getType();

        return new GetAllRespons((Map<String, Score>) (new Gson()).fromJson(json, type));
    }
}
