package be.samclercky.httpclient.exampleactions;

public class Score implements Comparable<Score> {
    public String name = "";
    public int score = 0;

    @Override
    public String toString() {
        return "Score{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(Score score) {
        return score.score - this.score;
    }
}
