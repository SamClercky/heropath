package be.samclercky.httpclient.exampleactions.respons;

import be.samclercky.httpclient.exampleactions.Score;

import java.util.Map;

public class GetAllRespons {

    public Map<String, Score> data;

    public GetAllRespons(Map<String, Score> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "GetAllRespons{" +
                "data=" + data +
                '}';
    }
}
