package be.samclercky.httpclient.exampleactions.respons;

import java.util.Objects;

/**
 * Representation of data coming from the server after addition
 */
public class AddRespons {
    public String name;

    public AddRespons(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AddRespons{" +
                "name='" + name + '\'' +
                '}';
    }
}
