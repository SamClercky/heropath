package be.samclercky.httpclient.actions;

import be.samclercky.httpclient.HttpEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Describes a action to be performed on the server
 */
public interface Action {

    /**
     * The url that should be called upon
     * @return The url
     */
    String getURL();

    ActionTypes getActionType();

    /**
     * Gets the serialized JSON data that needs to be send to the server
     * @return The JSON data string
     */
    default String getRawDataToBeSend() { return ""; }

    /**
     * Gets the Form data that needs to be send to the server
     * @return The Form data
     */
    default Map<String, Object> getFormData() {
        return new HashMap<>();
    }

    /**
     * Listener which receives all the events
     * @return The listener
     */
    HttpEventListener getListener();

    /**
     * Transforms the data coming from the server to a datastructure
     * @param json The JSON data coming from the server
     * @return The parsed data from the server
     */
    Object getDataFromRequestedJSON(String json);
}
