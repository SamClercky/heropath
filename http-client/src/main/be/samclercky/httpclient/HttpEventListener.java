package be.samclercky.httpclient;

/**
 * Listener for specific events
 * Callbacks will be called in the main thread
 */
public interface HttpEventListener {
    default void onRequestStarted() {} // empty default
    default void onRequestCanceled() {} // empty default
    default void onRequestFinished(Object result) {}
}
