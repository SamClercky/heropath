# Http-client

This is a simple, yet flexible http client to send and receive
messages from Google Firebase. It is initially intended for the
high scores in the game HeroPath.

## Why we did not use the library from our professor
I (SamClercky) am fascinated by the web and have already worked with
Google Firebase in the past. For me, this library is not only a part of the game,
but is written with modularity in mind, so I would be able to reuse the code for
later projects.

At the moment of writing we could not find any alternative libraries
for the java language (except for android, but it uses android specific
stuff and except the admin system which is not ideal for a game like this).

Doing this also shows we can read the documentation of REST-api's and
make something useful with it.

## Example usage

A way to view how to use this library can be found
[here](https://gitlab.com/SamClercky/heropath/-/blob/master/http-client/src/main/be/samclercky/httpclient/exampleactions/README.md).