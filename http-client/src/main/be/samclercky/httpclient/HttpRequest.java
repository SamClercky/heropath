package be.samclercky.httpclient;

import be.samclercky.httpclient.actions.Action;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;

class HttpRequest implements Callable<String> {
    private Action action;

    public HttpRequest(Action action) {
        this.action = action;
    }

    /**
     * Make the call to the api
     * @return The returned JSON
     */
    @Override
    public String call() {
        // build request
        HttpUriRequest request;

        switch (action.getActionType()) {
            case GET:
                request = new HttpGet(action.getURL());
                break;
            case POST:
                HttpPost httpPost = new HttpPost(action.getURL());

                // Raw data / json
                StringEntity jsonData = new StringEntity(
                        action.getRawDataToBeSend(),
                        ContentType.APPLICATION_JSON
                );

                if (action.getRawDataToBeSend() != "") { // prevent default data to be sent
                    httpPost.setEntity(jsonData);
                }

                // Form data
                List<NameValuePair> formData = new ArrayList<>();
                for (String key: action.getFormData().keySet()) {
                    formData.add(new BasicNameValuePair(key, action.getFormData().get(key).toString()));
                }

                if (formData.size() > 0) {
                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(formData));
                    } catch (UnsupportedEncodingException ex) {
                        ex.printStackTrace();
                        return ""; // end with default output
                    }
                }

                request = httpPost;
                break;
            default:
                throw new IllegalStateException("Unsupported action type: " + action.getActionType());
        }

        // Get respons
        StringBuilder respons = new StringBuilder();
        try(CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(request)) {

            // System.out.println(response.getStatusLine());
            HttpEntity entity = response.getEntity();

            InputStream in = entity.getContent();

            try (Scanner scanner = new Scanner(in, StandardCharsets.UTF_8.name())) {
                while (scanner.hasNext()) {
                    respons.append(scanner.next());
                }
            }

            EntityUtils.consume(entity);
        } catch (IOException ex) {
            ex.printStackTrace();
            return ""; // end with default output
        }

        return respons.toString();
    }
}
