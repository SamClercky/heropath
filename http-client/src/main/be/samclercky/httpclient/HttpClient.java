package be.samclercky.httpclient;

import be.samclercky.httpclient.actions.Action;

import java.io.Closeable;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class HttpClient implements Closeable {

    private LinkedList<Action> upcomingTasks = new LinkedList<>();
    private FutureTask<String> currentTask = null;
    private Action currentAction = null;

    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    /**
     * Submits for execution and starts immediately if there is a place ready
     * @param task The task to be executed
     */
    public void addTask(Action task) {
        upcomingTasks.add(task);
        cycleToNextInQueueIfAny();
    }

    private void cycleToNextInQueueIfAny() {
        if (upcomingTasks.size() != 0 &&
                (currentTask == null ||
                        currentTask.isDone() ||
                        currentTask.isCancelled())) {
            Action task = upcomingTasks.pollFirst();
            currentTask = new FutureTask<>(new HttpRequest(task));
            currentAction = task;

            // Start the task
            currentTask.run(); // Set result of calculation to result of the task
            executorService.execute(currentTask);

            currentAction.getListener().onRequestStarted();
        }
    }

    /**
     * Checks the current status of the latest request
     * @return If you should wait any longer for the request to complete
     */
    public boolean isBusy() {
        if (currentTask == null && currentAction == null) return false;

        if (currentTask.isCancelled()) {
            currentAction.getListener().onRequestCanceled();
            makeReadyForNextTask(currentAction);
            return false;
        } else if (currentTask.isDone()) {
            try {
                currentAction
                        .getListener()
                        .onRequestFinished(currentAction.getDataFromRequestedJSON(currentTask.get()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } finally {
                makeReadyForNextTask(currentAction);
            }

            return false;
        } else {
            return true;
        }
    }

    /**
     * Prevents memory leaks and accidentally overwriting current tasks
     * @param prevAction The previous action that is done/cancelled
     */
    private void makeReadyForNextTask(Action prevAction) {
        if (prevAction == currentAction) { // Safety check
            // Release for GC
            currentAction = null;
            currentTask = null;

            cycleToNextInQueueIfAny();
        }
    }

    @Override
    public void close() {
        // System.out.println("Http-client shutting down");
        upcomingTasks.clear();
        if (currentTask != null) {
            currentTask.cancel(true);
        }
        if (!executorService.isShutdown()) {
            executorService.shutdown();
        }

        // System.out.println("Http-client just cleared all resources");
    }
}
