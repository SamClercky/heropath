package be.samclercky.httpclient;

import be.samclercky.httpclient.exampleactions.GetAllAction;
import be.samclercky.httpclient.exampleactions.respons.GetAllRespons;

class HttpClientGetTest {
    static void testHttpClient() {
        HttpClient client = new HttpClient();

        GetAllAction action = new GetAllAction(new HttpEventListener() {
            @Override
            public void onRequestStarted() {
                System.out.println("Request started");
            }

            @Override
            public void onRequestCanceled() {
                System.out.println("Request canceled");
            }

            @Override
            public void onRequestFinished(Object result) {
                assert result instanceof GetAllRespons;
                System.out.print("The result is: ");
                System.out.println(result);
            }
        });
        client.addTask(action);

        while (client.isBusy());

        System.out.println("Execution ended");

        client.close();
    }

    public static void main(String[] args) {
        testHttpClient();
    }
}
