package be.samclercky.httpclient;

import be.samclercky.httpclient.exampleactions.AddAction;
import be.samclercky.httpclient.exampleactions.GetAllAction;
import be.samclercky.httpclient.exampleactions.Score;
import be.samclercky.httpclient.exampleactions.respons.AddRespons;

public class HttpClientPostTest {
    static void testHttpClient() {
        HttpClient client = new HttpClient();

        Score data = new Score();
        data.name = "Poempernikkel";
        data.score = 2;

        System.out.println("Http-client is going to send: " + data.toString());

        AddAction action = new AddAction(new HttpEventListener() {
            @Override
            public void onRequestStarted() {
                System.out.println("Request started");
            }

            @Override
            public void onRequestCanceled() {
                System.out.println("Request canceled");
            }

            @Override
            public void onRequestFinished(Object result) {
                assert result instanceof AddRespons;
                System.out.print("The result is: ");
                System.out.println(result);
            }
        }, data);
        client.addTask(action);

        while (client.isBusy());

        System.out.println("Execution ended");

        client.close();
    }

    public static void main(String[] args) {
        testHttpClient();
    }
}
