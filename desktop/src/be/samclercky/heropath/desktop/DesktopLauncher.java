package be.samclercky.heropath.desktop;

import be.samclercky.heropath.Constants;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import be.samclercky.heropath.HeroPath;

public class DesktopLauncher {
	static final int INITIAL_SIZE_FACTOR = 4;

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.addIcon("icons/icon-256.png", Files.FileType.Internal);
		config.addIcon("icons/icon-64.png", Files.FileType.Internal);
		config.addIcon("icons/icon-32.png", Files.FileType.Internal);

		config.width = Constants.V_WIDTH * INITIAL_SIZE_FACTOR;
		config.height = Constants.V_HEIGHT * INITIAL_SIZE_FACTOR;
		config.fullscreen = Constants.START_IN_FULLSCREEN;

		new LwjglApplication(new HeroPath(), config);
	}
}
