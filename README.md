# HeroPath
Made with :heart: by **[OsamaBinNaughty](https://gitlab.com/OsamaBinNaughty)** & **[SamClercky](https://gitlab.com/SamClercky)**

## Table of contents

* [Preface](#preface)
* [How to build](#how-to-build)
    * [Eclipse](#eclipse)
    * [Intellij IDEA](#intellij-idea)
    * [Command line](#command-line)
* [Dependencies](#our-dependencies)
* [Features](#features)
* [Google Firebase](#google-firebase)
* [Gitlab Repository](#gitlab-repository)

## Preface

This game is a project for our java-course at the Free University Brussels.

HeroPath is a level based game themed around kings and pigs. All the code
in the project files is written by us unless you see the **author** tag in the javadoc.

## How to build

***IMPORTANT: LibGDX is a library written for java 7. Other versions sometimes work, but are not guaranteed***

We wrote all the code in IntelliJ IDEA and with the help of `gradle`.


`gradle` is a build system which can be found at [https://gradle.org/](https://gradle.org/), but is
in our experience easier to install through [SDKMan](https://sdkman.io/install).
Note that the installation of `gradle` is not required if you use IntelliJ IDEA, since they 
support it out of the box.

After installing SDKMan, issue the following command:
`$> sdk install gradle`

### Eclipse

1. Download Eclipse
2. Download the Gradle-plugin
3. Download git for your system
4. Clone the repository on to your system: `git clone git@gitlab.com:SamClercky/heropath.git`
5. Open a existing project in Eclipse and chose `gradle` as build system
6. Wait till eclipse has indexed and downloaded all the dependencies
7. Hit run (main class is in desktop/src/be/samclercky/heropath/desktop/DesktopLauncher.java)

### IntelliJ IDEA

1. Install IntelliJ IDEA (community edition will suffice)
2. Install the `git` plugin (most of the time preinstalled)
3. Click in the opening screen: open from git
4. IntelliJ will now ask for the following git url: `git@gitlab.com:SamClercky/heropath.git`
5. When asked for a build system, chose gradle
6. Hit run (main class is in desktop/src/be/samclercky/heropath/desktop/DesktopLauncher.java)

### Command line

```
$> cd <path to to where you want the project to be installed>
$> git clone git@gitlab.com:SamClercky/heropath.git
$> cd heropath
$> ./gradlew run --refresh-dependencies # in subsequent runs --refresh-dependencies can be omitted
```

***Windows users need to use `./gradlew.bat` instead of just `./gradlew`,
if this does not work for you, you can try [Windows Subsystem for Linux](https://docs.microsoft.com/nl-nl/windows/wsl/install-win10)***

## Our dependencies

All depedencies are listed in the gradle files, but here is a shortend list:
* [LibGDX (Game library](https://github.com/libgdx/libgdx)
* [LibGDX controller support](https://github.com/libgdx/libgdx/wiki/Controllers)

### Textures
* [Kings and Pigs](https://pixel-frog.itch.io/kings-and-pigs)
* [Magic cliffs environment](https://ansimuz.itch.io/magic-cliffs-environment)
* [MenuScreen Pixelart](https://www.reddit.com/r/PixelArt/comments/5z0j67/oc_night_castle/)
* [Throw Stone Pixelart](http://pixelartmaker.com/art/8fd9223acb89db4)

### Music
* [Fantasy Celtic Music](https://www.youtube.com/watch?v=g-jGHbkM8e4)
* [Kyrandia - Timbermist Woods](https://www.youtube.com/watch?v=oN8WTvKGVqI&list=PLEC60FE0D5A8A8AFF)

## Features

* Google Firebase database for real-time highscores
* Support for XBox 360 controllers
* Helpful help screen
* Fun game
* Funny death messages
* Debug mode (with flags for almost everything)
* And sooo much more :)

## Google Firebase
More information about this topic, you can find [here](https://gitlab.com/SamClercky/heropath/-/blob/master/http-client/src/main/be/samclercky/httpclient/README.md)

## Gitlab Repository
You can find our gitlab repo [here](https://gitlab.com/SamClercky/heropath).