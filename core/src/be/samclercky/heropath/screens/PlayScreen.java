package be.samclercky.heropath.screens;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.HeroPath;
import be.samclercky.heropath.behavior.KillableBehavior;
import be.samclercky.heropath.overlay.Hud;
import be.samclercky.heropath.sprites.dynamics.PlayerObject;
import be.samclercky.heropath.utils.GamePadManager;
import be.samclercky.heropath.utils.WorldManager;
import be.samclercky.heropath.utils.XBox360Pad;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class PlayScreen implements Screen {

    public enum State {PAUSE, RUN, RESUME}

    private State state = State.RUN;

    private OrthographicCamera gameCam;
    private Viewport gamePort;

    private HeroPath game;

    private WorldManager worldManager;

    private PlayerObject player;

    private Hud hud;

    private TextureAtlas KingAtlas;
    private TextureAtlas PigAtlas;
    private TextureAtlas PigKingAtlas;
    private TextureAtlas BulletAtlas;

    private Music music;

    // delay variables for GameOverScreen
    private float timer = 3f;
    private float time = 0;
    private boolean didPlayerFinish = false;

    /**
     * Constructor
     * @param game The HeroPath game
     */
    public PlayScreen(HeroPath game) {
        this.game = game;

        //define the atlas
        KingAtlas = new TextureAtlas("Graphics/Animation/king/King_HeroPath.pack");
        PigAtlas = new TextureAtlas("Graphics/Animation/pig/Pig_HeroPath.pack");
        BulletAtlas = new TextureAtlas("Graphics/Animation/bullet/Bullet_HeroPath.pack");
        PigKingAtlas = new TextureAtlas("Graphics/Animation/pigKing/PigKing_HeroPath.pack");

        //cam that follows our player
        gameCam = new OrthographicCamera();

        //FitviewPort for virtual aspect
        gamePort = new FitViewport(Constants.V_WIDTH / Constants.PPM,
                Constants.V_HEIGHT / Constants.PPM,
                gameCam
        );

        //make our HUD
        hud = new Hud(game.getBatch(), this);

        gameCam.position.set(gamePort.getWorldWidth() / 2,
                gamePort.getWorldHeight() / 2, 0);

        worldManager = new WorldManager(this);

        // Setup gamepad support
        new GamePadManager();
        player = new PlayerObject(worldManager.getWorld(), this);

        playMusic();
    }

    /**
     * getters: Atlas for Pig and King
     * @return KingAtlas / PigAtlas
     */
    public TextureAtlas getKingAtlas(){ return KingAtlas; }
    public TextureAtlas getPigAtlas(){ return  PigAtlas; }
    public TextureAtlas getPigKingAtlas() { return PigKingAtlas; }
    public TextureAtlas getBulletAtlas(){ return BulletAtlas; }

    /**
     * Needed for enabling special collision detection
     * @return The killableBehavior from the Player
     */
    public KillableBehavior getPlayerHandler() {
        if (player == null) return null;

        return player.getBehaviorHandler();
    }

    /**
     * Game music setup
     * Debug mode for the music
     */
    private void playMusic(){
        music = game.getAssetManager().get("audio/music/Kyrandia-TimbermistWoods.mp3", Music.class);
        music.setVolume(0.2f);
        music.setLooping(true);

        if (!Constants.DISABLE_MUSIC) {
            music.play();
        }
    }

    /**
     * Add score to 'current' score
     * @param score that will be added
     */
    public void addScore(int score) {
        hud.addScore(score);
    }

    /**
     * Signal player won
     */
    public void playerWon() {
        hud.addWinScore();
        didPlayerFinish = true;
    }

    /**
     * Sets the state the player is in
     * @param s State that the game is currently in
     */
    public void setGameState(State s){
        this.state = s;
    }

    /**
     * Gives a reference to the global game object
     * @return The global game object
     */
    public HeroPath getGame() {
        return game;
    }

    /**
     * GameOver logic
     * @return boolean if game over should be activated
     */
    private boolean gameOver(){
        return getPlayerHandler().isDead();
    }

    /**
     * Logic when pause can be activated
     * @return returns the pause State
     */
    private boolean shouldGoToPause() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE) || GamePadManager.buttonJustPressed(XBox360Pad.BUTTON_START)) {
            setGameState((state == State.RUN)? State.PAUSE : State.RUN);
        }

        return state == State.PAUSE;
    }

    /**
     * You can't swing if you don't have stamina --> show red display on screen
     */
    public void onNotEnoughStamina() {
        hud.onShowStamina();
    }

    /**
     * Update all positional elements before rendering
     * @param delta The time in seconds since the last render.
     */
    private void update(float delta) {
        boolean shouldPause = shouldGoToPause();

        if (!shouldPause) {
            player.onUpdate(delta);
            worldManager.onUpdate(delta, gameCam);

            // Follow player
            gameCam.position.x = player.getX();
            gameCam.update();

        }

        hud.update(delta, shouldPause);
        GamePadManager.onUpdate(delta);
    }

    /**
     * Called when this screen becomes the current screen for a Game.
     * Sets nothing as inputProcessor that will receive all touch and key input events.
     */
    @Override
    public void show() {
        // Needed for the HUD to work
        hud.acquireInputProcessor();
    }

    /**
     * Called when the screen should render itself.
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {
        //update method
        update(delta);

        // Clears screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //render worldmanager
        worldManager.onRender(gameCam);

        //Sets the projection matrix to be used by this Batch.
        game.getBatch().setProjectionMatrix(gameCam.combined);

        //draw sprites
        game.getBatch().begin();
        worldManager.onEnemiesDraw(game.getBatch());
        player.draw(game.getBatch());
        //bulletObject.draw(game.getBatch());
        game.getBatch().end();

        //set our batch to now draw what the Hud camera sees
        game.getBatch().setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

        //game over delay, so the animations finishes
        if (Constants.ENABLE_GAME_OVER) {
            if (gameOver() || didPlayerFinish) {
                time += delta;
                if (time >= timer) {
                    game.setScreen(new GameOverScreen(game, hud.getScore(), didPlayerFinish));
                    dispose();
                }
            }
        }
    }

    /**
     * Called when the Application is resized.
     * Update gamePort with given width and height
     * @param width the new width in pixels
     * @param height the new height in pixels
     */
    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    /**
     * Called when the Application is paused
     * Sets state to pause
     */
    @Override
    public void pause() {
        this.state = State.PAUSE;
    }

    /**
     * Called when the Application is paused
     * Sets state to resume
     */
    @Override
    public void resume() {
        this.state = State.RESUME;
    }

    /**
     * Called when this screen is no longer the current screen for a Game.
     */
    @Override
    public void hide() { }

    /**
     * Called when the Application is destroyed
     */
    @Override
    public void dispose() {
        music.dispose();
        hud.dispose();
    }
}
