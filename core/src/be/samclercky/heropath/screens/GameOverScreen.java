package be.samclercky.heropath.screens;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.HeroPath;
import be.samclercky.heropath.ui.BtnFactory;
import be.samclercky.heropath.ui.InputFactory;
import be.samclercky.heropath.ui.LblFactory;
import be.samclercky.heropath.utils.GamePadManager;
import be.samclercky.heropath.utils.Utils;
import be.samclercky.heropath.utils.XBox360Pad;
import be.samclercky.httpclient.exampleactions.Score;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

public class GameOverScreen implements Screen {
    private final Viewport viewport;
    private final Stage stage;
    private final HeroPath game;
    private final int endScore;

    private TextField usernameField;

    /**
     * Funny messages if you die
     */
    private final static String[] badMessages = {
            "Aw, I hope blood does not stick...",
            "NOOOB!",
            "Your spine looks like a chicken nugget",
            "I hope your mom wasn't looking!",
            "Stay focused! This is a serious game",
            "Wait I see light at the end...",
            "That must hurt, hope you have dafalgan!",
            "You won't win this game by losing",
            "You're dead. Again." ,
            "Time to reload.",
            "Hoped you saved your game, cause you're dead.",
            "Here's a picture of your corpse. Not pretty.",
            "Boy, are you stupid. And dead.",
            "Ha ha ha ha ha. You're dead, moron!",
            "Couldn't charm your way out of that one.",
            "Nope.",
            "This is why your friends don't let you play any games",
            "This is why you don't have any friends...",
            "You must like the color black, cause you're dead.",
            "I hope your IQ is a bit higher than your level...",
            "End of your story",
    };

    private static final String[] niceMessages = {
            "Nice...",
            "Congrats, I guess...",
            "You did great! "
    };

    /**
     * Constructor
     * @param game The HeroPath game
     * @param endScore end score if you die or complete a level
     * @param didPlayerFinish Did the player complete a level?
     */
    public GameOverScreen(final HeroPath game, int endScore,boolean didPlayerFinish){
        //define game
        this.game = game;
        this.endScore = endScore;

        //define viewport / stage
        viewport = new FitViewport(Constants.V_WIDTH, Constants.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, game.getBatch());

        //define table
        Table table = new Table();
        table.center();
        table.setFillParent(true);

        stage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!event.isStopped()) {
                    playAgain();
                }

                super.clicked(event, x, y);
            }
        });

        //define labels
        Label gameOverLabel = LblFactory.createMenuTitleLabel("GAME OVER");
        Label winLabel = LblFactory.createGreenWinLabel("YOU WIN");
        Label playAgainLabel = LblFactory.createGameOverLabel("CLICK TO RESTART");
        Label endScoreLabel = LblFactory.createGameOverLabel("YOUR SCORE: " + endScore);
        Label addToHighScore = LblFactory.createGameOverLabel("ADD NAME TO SCORE BOARD");

        //define random messages Label
        Random rand = new Random();
        Label loseMessage = LblFactory.createFunnyMessageLabel(badMessages[rand.nextInt(badMessages.length)]);
        Label winMessage = LblFactory.createFunnyMessageLabel(niceMessages[rand.nextInt(niceMessages.length)]);

        usernameField = createTextField();

        //table add
        menuButton(table);
        table.row();
        table.add(endScoreLabel).colspan(2).expandX().padTop(30f);
        table.row();
        table.add(didPlayerFinish ? winLabel : gameOverLabel).colspan(2).expandX();
        table.row();
        table.add(didPlayerFinish ? winMessage : loseMessage).colspan(2).expandX();
        table.row();
        table.add(addToHighScore).colspan(2).expandX().padTop(20f);
        table.row();
        table.add(usernameField).expandX();
        table.add(createHighScoreSubmitBtn()).expandX();
        table.row();
        table.add(playAgainLabel).colspan(2).expandX().padTop(25f);

        table.row();
        table.add(LblFactory.createEmpty()).colspan(2).expandY();
        table.row();

        //add table to stage
        stage.addActor(table);

        //debug mode for tables
        if (Constants.ENABLE_UI_DEBUG) {
            table.setDebug(true);
        }
    }

    /**
     * Return to the menu, while you are dead or completed the level
     * @param table used in the GameOverScreen
     */
    private void menuButton(Table table){
        TextButton menuBtn = BtnFactory.createLessPoppingButton("MENU");
        menuBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                event.stop();
                goToMenu();
            }
        });
        table.add(menuBtn).colspan(2).padTop(15f);
    }

    /**
     * Input handle for the gamePad and PC
     * @param delta The time in seconds since the last render.
     */
    private void inputHandle(float delta){
        GamePadManager.onUpdate(delta);

        if (GamePadManager.buttonJustPressed(XBox360Pad.BUTTON_START)) { // handle Gamepad
            playAgain();
        } else if (GamePadManager.buttonJustPressed(XBox360Pad.BUTTON_BACK)) {
            goToMenu();
        }

    }

    /**
     * Go to MenuScreen
     */
    private void goToMenu() {
        goToMenu(MenuScreen.MenuState.Menu);
    }

    /**
     * Go to MenuScreen
     * @param menuState wich MenuScreen (Menu, Credits, ...)
     */
    private void goToMenu(MenuScreen.MenuState menuState) {
        Utils.transitionToScreen(game, this, new MenuScreen(game, menuState));
    }

    /**
     * Transition from GameOverScreen to PlayScreen
     */
    private void playAgain() {
        Utils.transitionToScreen(game, this, new PlayScreen(game));
    }

    /**
     * Called when this screen becomes the current screen for a Game.
     * Sets the stage as inputProcessor that will receive all touch and key input events.
     */
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Called when the screen should render itself.
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {
        //clear screen
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //draw stage
        stage.draw();

        inputHandle(delta);
    }

    /**
     * Create TextField for scene2D
     * @return result of the TextField
     */
    private TextField createTextField() {
        TextField result = InputFactory.createSimpleTextField(" Username");
        result.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                event.stop();
                super.clicked(event, x, y);
            }
        });

        return result;
    }

    /**
     * Button for submitting your highscore in the database
     * @return result
     */
    private TextButton createHighScoreSubmitBtn() {
        TextButton result = BtnFactory.createMenuButton("ADD");
        result.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                event.stop();
                Score score = new Score();
                score.name = usernameField.getText();
                score.score = endScore;
                game.getHttpClientManager().addNewData(score);
                goToMenu(MenuScreen.MenuState.HighScores);
                super.clicked(event, x, y);
            }
        });

        return result;
    }

    /**
     * Called when the Application is resized.
     * @param width the new width in pixels
     * @param height the new height in pixels
     */
    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() { }

    @Override
    public void resume() { }

    /**
     * Called when this screen is no longer the current screen for a Game.
     */
    @Override
    public void hide() { }

    /**
     * Called when the Application is destroyed.
     */
    @Override
    public void dispose() {
        stage.dispose();
    }
}
