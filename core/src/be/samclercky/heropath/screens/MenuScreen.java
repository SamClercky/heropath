package be.samclercky.heropath.screens;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.HeroPath;
import be.samclercky.heropath.ui.BtnFactory;
import be.samclercky.heropath.ui.LblFactory;
import be.samclercky.heropath.utils.Utils;
import be.samclercky.httpclient.exampleactions.Score;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Arrays;
import java.util.PriorityQueue;

public class MenuScreen implements Screen {
    private HeroPath game;

    private Stage stage;
    private Stack root;
    private Table currentTable;

    private boolean needsCheckingForHighScoreAvailability = true;
    private MenuState currentState = MenuState.Menu;

    private Music music;

    public enum MenuState{Menu, Help, Credits, AboutUs, HighScores}

    public MenuScreen(HeroPath game) {
        this(game, MenuState.Menu);
    }

    /**
     * Constructor
     * @param game The HeroPath game
     * @param initialState First state you want to see, when you go to the MenuScreen
     */
    public MenuScreen(HeroPath game, MenuState initialState) {
        this.game = game;

        //define stage
        stage = new Stage(new FitViewport(Constants.V_WIDTH,
                Constants.V_HEIGHT, new OrthographicCamera()), game.getBatch());

        addRootToStage();

        addBackGround();

        sendNewState(initialState);

        playMusic();
    }

    /**
     * Changes screens
     * @param to what Screen should it go?
     */
    private void transitionToScreen(Screen to) {
        Utils.transitionToScreen(game, this, to);
    }

    /**
     * Adds the root to the stage
     * Actor is sized to the parent
     */
    private void addRootToStage(){
        root = new Stack();
        stage.addActor(root);
        root.setFillParent(true);
    }

    /**
     * Menu music setup
     * Debug mode for the music
     */
    private void playMusic(){
        music = game.getAssetManager().get("audio/music/fantasy1.mp3", Music.class);
        music.setVolume(0.2f);
        music.setLooping(true);

        if (Constants.DISABLE_MUSIC == false) {
            music.play();
        }
    }

    /**
     * Locate the Texture of the background
     * Make the Texture an Image
     * add Image to root
     */
    private void addBackGround(){
        Texture backgroundTexture = new Texture("Graphics/Photos/castle_pixel_art.png");
        Image backgroundImage = new Image(backgroundTexture);
        root.add(backgroundImage);
    }

    /**
     * Check if the root is bigger then 1 --> Delete previous table / Set new currenTable
     * add the 'new' table
     * Invalidate the actor's layout
     * @param table used
     */
    private void addToRoot(Table table) {
        //deletes the previous added table
        if (root.getChildren().size > 1){
            root.getChildren().pop();
            currentTable = table;
        }

        root.add(table);

        root.invalidate();
    }

    /**
     * Create a whiteSpace in the Y-direction (frequently used method)
     * @param table The table that needs extra spacing
     * @param isBig true ==> big spacing, false ==> small spacing
     */
    private void whiteSpace_Y(Table table, boolean isBig){
        table.row();

        if (isBig) {
            table.add(LblFactory.createEmpty()).expandY();
            table.row();
        }
    }

    /**
     * create the Menu layout
     */
    private void createMenu() {
        //table setup
        Table table = setupNewTable("HEROPATH", 3);

        whiteSpace_Y(table, false);

        //add START,HELP,QUIT buttons
        addMidButtons(table);

        whiteSpace_Y(table, false);

        //add CREDITS,ABOUT US buttons
        addLowButtons(table);
    }

    /**
     * Sends a new MenuState so the layout is called in the constructor
     * @param newMenuState is the new layout state
     * @throws IllegalStateException if a illegal state has been submitted
     */
    private void sendNewState(MenuState newMenuState) throws IllegalStateException{
        switch (newMenuState) {
            case Menu:
                createMenu();
                break;
            case Help:
                createHelp();
                break;
            case Credits:
                createCredits();
                break;
            case AboutUs:
                createAboutUs();
                break;
            case HighScores:
                createHighScore();
                break;
            default:
                throw new IllegalStateException("Found illegal state: " + newMenuState);
        }

        currentState = newMenuState;
    }

    /**
     *  Setup for the back to the menu button
     * @param table used
     */
    private void addBackToMenuButton(Table table, int colspan){
        TextButton backBtn = BtnFactory.createMenuButton("BACK");
        backBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                sendNewState(MenuState.Menu);
            }
        });

        table.add(backBtn).colspan(colspan).padBottom(10f);
    }

    /**
     * Create the Help layout
     */
    private void createHelp(){
        //Table setuo
        Table table = setupNewTable("HELP", 1);

        whiteSpace_Y(table, true);

        String helpText = "Moving: use arrow keys or ZQSD/WASD\n" +
                "Kill enemies by jumping on them and crushing them with your rock! \n" +
                "Use SPACE key to be an earth bender! \n";
        table.add(LblFactory.createBlockText(helpText)).width(Constants.V_WIDTH - 50);

        whiteSpace_Y(table, true);

        //Back to Menu button
        addBackToMenuButton(table, 1);
    }

    /**
     * Create the About Us Layout
     */
    private void createAboutUs(){
        //Table setup
        Table table = setupNewTable("ABOUT US", 1);

        whiteSpace_Y(table, true);

        Label aboutUs = LblFactory.createBlockText("Dries and Andreas started HeroPath because we wanted to have good grades on our Java course. " +
                "That's why we made a fun little game with amazing graphics called HeroPath. " +
                "Along the way, we shared a lot of joy and sometimes a couple of tears. " +
                "We want to be engineers one day, but first we needed to crush this java project.");

        table.add(aboutUs).width(Constants.V_WIDTH - 50);

        whiteSpace_Y(table, true);

        //Back to Menu button
        addBackToMenuButton(table, 1);
    }

    /**
     * Create the Credits layout
     */
    private void createCredits(){
        Table table = setupNewTable("CREDITS", 1);

        whiteSpace_Y(table, true);

        //Label init
        Label design = LblFactory.createBlockText("Design: Dries & Andreas");
        Label scripts = LblFactory.createBlockText("Scripts: Andreas & Dries");
        Label animation = LblFactory.createBlockText("Animation: Dries & Andreas");
        Label graphics = LblFactory.createBlockText("Graphics: internet");
        Label guidance = LblFactory.createBlockText("Guidance: Free University of Brussels");

        //Add labels to the table
        table.add(design).width(Constants.V_WIDTH - 50).expandX();
        table.row();
        table.add(scripts).width(Constants.V_WIDTH - 50).padTop(10f);
        table.row();
        table.add(animation).width(Constants.V_WIDTH - 50).padTop(10f);
        table.row();
        table.add(graphics).width(Constants.V_WIDTH - 50).padTop(10f);
        table.row();
        table.add(guidance).width(Constants.V_WIDTH - 50).padTop(10f);

        whiteSpace_Y(table, true);

        //Back to the Menu button
        addBackToMenuButton(table, 1);
    }

    /**
     * Create the Highscore layout
     */
    private void createHighScore() {
        // Setup table
        Table table = setupNewTable("HIGH SCORES", 3);

        table.row();

        Array<Score> highScores = game.getHttpClientManager().getHighScores();

        if (highScores != null) {

            //Score[] data = Arrays.copyOf(highScores.toArray(), Math.min(highScores.size(), 10), Score[].class);
            for (int i = 0; i < highScores.size; i++) { // Show 10 best scores
                Label posLbl = LblFactory.createHighScoresLabel(String.format("%02d", i+1));
                Label nameLbl = LblFactory.createHighScoresLabel(highScores.get(i).name);
                Label scoreLbl = LblFactory.createHighScoresLabel(String.format("%07d", highScores.get(i).score));

                table.add(posLbl).expandX();
                table.add(nameLbl).expandX();
                table.add(scoreLbl).expandX();

                table.row();
            }
        } else {
            Label loadingLbl = LblFactory.createHighScoresLabel("Loading...");
            table.add(loadingLbl).expandX();
            table.row();
        }

        addBackToMenuButton(table, 3);
    }

    /**
     *  Logic for START / HELP / QUIT buttons
     *  Start button goes to new PlayScreen and stops music
     *  Help button sends Help MenuState
     *  Quit button stops GDX app
     * @param table used
     */
    private void addMidButtons(Table table) {
        // init
        TextButton startBtn = BtnFactory.createMenuButton("START");
        TextButton highScoreBtn = BtnFactory.createMenuButton("SCORES");
        TextButton helpBtn = BtnFactory.createMenuButton("HELP");
        TextButton quitBtn = BtnFactory.createMenuButton("QUIT");

        // listeners
        startBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                music.stop();
                transitionToScreen(new PlayScreen(game));
            }

        });

        highScoreBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                sendNewState(MenuState.HighScores);
            }
        });
        helpBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                sendNewState(MenuState.Help);
            }
        });

        quitBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        // add to table
        table.add(startBtn).expandX().colspan(2);
        table.row();
        table.add(highScoreBtn).expandX().padTop(5f).colspan(2);
        table.row();
        table.add(helpBtn).expandX().padTop(5f).colspan(2);
        table.row();
        table.add(quitBtn).expandX().padTop(5f).colspan(2);
    }

    /**
     * Logic for the CREDITS /ABOUT US buttons
     * Credits button send Credits MenuState
     * About Us button send AboutUs MenuState
     * @param table used
     */
    private void addLowButtons(Table table){
        TextButton creditsBtn = BtnFactory.createLessPoppingButton("CREDITS");
        TextButton aboutUsBtn = BtnFactory.createLessPoppingButton("ABOUT US");

        creditsBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                sendNewState(MenuState.Credits);
            }
        });

        aboutUsBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                sendNewState(MenuState.AboutUs);
            }
        });

        table.add(creditsBtn).expandX().pad(0,0,10,60);
        table.add(aboutUsBtn).expandX().pad(0,60,10,0);

    }

    /**
     * Creation of new table with a few default settings
     * @param title The title of the new window
     * @param colspan Number of all columns in the window
     * @return The freshly created table
     */
    private Table setupNewTable(String title, int colspan) {
        //Table setup
        Table table = new Table();
        table.setFillParent(true);
        addToRoot(table);

        // Debug lines scene2D
        if (Constants.ENABLE_UI_DEBUG)
            table.setDebug(true);

        //Add title Label to table
        table.add(LblFactory.createMenuTitleLabel(title)).colspan(colspan).expandX().center().padTop(10);

        return table;
    }

    /**
     * Called when this screen becomes the current screen for a Game.
     * Sets the stage as inputProcessor that will receive all touch and key input events.
     * if true, go straight to PlayScreen
     */
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

        // DEBUGGING
        if (!Constants.ENABLE_MENU)
            transitionToScreen(new PlayScreen(game));
    }

    /**
     * Called when the screen should render itself.
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {
        // Clear screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw UI
        stage.act(delta);
        stage.draw();

        // Check for high score
        if (needsCheckingForHighScoreAvailability &&
                game.getHttpClientManager().getHighScores() != null) {

            needsCheckingForHighScoreAvailability = false;
            if (currentState == MenuState.HighScores)
                sendNewState(MenuState.HighScores); // reload
        }

    }

    @Override
    public void pause() { }


    @Override
    public void resume() { }

    /**
     * Called when this screen is no longer the current screen for a Game.
     */
    @Override
    public void hide() { }

    /**
     * Called when the Application is resized.
     * @param width the new width in pixels
     * @param height the new height in pixels
     */
    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    /**
     * Called when the Application is destroyed.
     */
    @Override
    public void dispose() {
        stage.dispose();
        music.dispose();
    }

}
