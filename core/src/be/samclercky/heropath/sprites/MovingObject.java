package be.samclercky.heropath.sprites;

import be.samclercky.heropath.behavior.MovingBehavior;
import be.samclercky.heropath.utils.LazyInit;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public abstract class MovingObject
        extends Sprite
        implements MovingBehavior.MovingListener, Disposable {
    protected Rectangle bounds;
    protected World world;
    protected Vector2 spawnLocation;

    private float stateTimer = 0;  //keep track how long we are in an animation

    private final LazyInit<Body> body;
    private final MovingBehavior movingBehavior;

    private boolean readyToBeRemoved = false;
    private boolean isFacingLeft = false; // remembers direction when player is standing still

    public MovingObject(Rectangle bounds, World world, TextureRegion textureRegion, Vector2 spawnLocation) {
        super(textureRegion);

        this.bounds = bounds;
        this.world = world;
        this.spawnLocation = spawnLocation;

        setBounds(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());

        setupAnimation();

        body = createBody();
        movingBehavior = new MovingBehavior(this);
    }

    public void onUpdate(float delta) {
        stateTimer += delta;

        if (movingBehavior != null) {
            movingBehavior.onUpdate(delta);
        }

        setRegion(getFrame(delta));
    }

    public World getWorld() {
        return world;
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public void onShouldResetPosition(float x, float y) {
        setPosition(x, y);
    }

    // Specific implementation for Body specific subclasses
    public abstract LazyInit<Body> createBody();

    /**
     * Gets the current body
     * @return Current body or null
     */
    public LazyInit<Body> getBody() {
        return body;
    }

    /**
     * Function where animations are initialized
     */
    protected abstract void setupAnimation();

    public Vector2 getSpawnLocation() {
        return spawnLocation;
    }

    /**
     * Generates a new animation from texture map
     * @param numberOfFrames Number of frames in texture map
     * @param startPos Start position on map
     * @param dimensions Dimensions of animation
     * @param isLoop Does the animation need to loop
     * @return The freshly created animation
     */
    protected Animation<TextureRegion> generateAnimation(int numberOfFrames, Vector2 startPos, Vector2 dimensions, boolean isLoop) {
        Array<TextureRegion> frames = new Array<>();

        for (int i = 0; i < numberOfFrames; i++) {
            frames.add(new TextureRegion(getTexture(),
                    (int)(i*dimensions.x + startPos.x), (int)(startPos.y), // iterate horizontal
                    (int)(dimensions.x), (int)(dimensions.y)));
        }

        return new Animation<>(
                0.1f,
                frames,
                isLoop ? Animation.PlayMode.LOOP : Animation.PlayMode.NORMAL
        );
    }

    /**
     * Method that specifies on every frame the correct part of an animation
     * @param delta Time passed since last call
     * @return The frame that should be rendered
     */
    protected abstract TextureRegion getFrame(float delta);

    /**
     * Method that specifies on every frame the correct part of an animation
     * @param region The selected animation for the current state
     * @param invert Whether flipping should be inverted
     * @return The correct frame that must be displayed
     */
    protected TextureRegion getFrame(Animation<TextureRegion> region, boolean invert) {
        return getFrame(region.getKeyFrame(getStateTimer()), invert);
    }

    /**
     * Method that specifies on every frame the correct part of an animation
     * @param region The selected frame animation for the current state
     * @param invert Whether flipping should be inverted
     * @return The correct frame that must be displayed
     */
    protected TextureRegion getFrame(TextureRegion region, boolean invert) {
        return flipFrame(region, invert);
    }

    /**
     * Convenience method to automatic flip an animation based on the b2body
     * @param region The region to be flipped
     * @param invert Whether or not the logic should be inverted
     * @return The correctly flipped Frame/Region
     */
    protected TextureRegion flipFrame(TextureRegion region, boolean invert) {
        if (getBody().get() != null) {
            boolean isSpeedNeg = getBody().get().getLinearVelocity().x == 0 ? isFacingLeft : getBody().get().getLinearVelocity().x < 0;
            isFacingLeft = isSpeedNeg;

            if (((isSpeedNeg^invert)) && !region.isFlipX()){
                region.flip(true, false);
            }
            else if(((!isSpeedNeg^invert)) && region.isFlipX()){
                region.flip(true,false);
            }
        }

        return region;
    }

    /**
     * Send signal to remove sprite
     */
    protected void makeReadyToRemove() {
        readyToBeRemoved = true;
    }

    /**
     * Method for onUpdate to see if sprite should be removed from the game
     * @return true if the sprite can be disposed
     */
    public boolean canDispose() {
        return readyToBeRemoved;
    }

    protected float getStateTimer() {
        return stateTimer;
    }

    protected void resetStateTimer() {
        stateTimer = 0;
    }

    /**
     * This method must be called to prevent memory leaks
     */
    @Override
    public void dispose() {
        getBody().dispose();
    }

    public Vector2 getCurrentPosition() {
        if (getBody().get() == null) return new Vector2(0, -100); // DEAD
        return getBody().get().getPosition();
    }

    public boolean isFacingLeft() {
        return isFacingLeft;
    }
}
