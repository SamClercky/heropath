package be.samclercky.heropath.sprites.dynamics;

import be.samclercky.heropath.behavior.Behavior;
import be.samclercky.heropath.sprites.MovingObject;
import be.samclercky.heropath.utils.LazyInit;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

/**
 * Representation of the dynamic sprites on screen
 * @param <S> State
 */
public abstract class DynamicObject<S> extends MovingObject {

    // Vars associated with animation
    private S currentState;

    private float removalDelay = 3f;
    protected boolean shouldStartRemovalDelay = false;

    public DynamicObject(Rectangle bounds, World world, TextureRegion textureRegion, Vector2 spawnLocation) {
        super(bounds, world, textureRegion, spawnLocation);
    }

    @Override
    public void onUpdate(float delta) {
        super.onUpdate(delta);

        if (shouldStartRemovalDelay) {
            removalDelay -= delta;

            if (removalDelay <= 0) {
                makeReadyToRemove();
            }
        }
    }

    /**
     * Accessor for state management
     * @param state The state where the object should transition to
     * @param isStrict Should acceptNewState() be called?
     */
    protected void setState(S state, boolean isStrict) {
        if (currentState != state && (!isStrict || acceptNewState(state))) {
            // reset stateTimer
            // System.out.println("stateTimer reset with new state: " + currentState);
            resetStateTimer();

            currentState = state;

            // Notify
            onStateChanged(state);
        }
    }

    /**
     * Accessor for state management
     * @param state The state where the object should transition to
     */
    public void setState(S state) {
        setState(state, true);
    }

    public S getCurrentState() {
        return currentState;
    }

    /**
     * Lifecycle method when state changes
     * Interesting when doing one time things like sound effects and particles
     * @param newState The new state
     */
    protected void onStateChanged(S newState) {
        // Default behavior has nothing to do
    }

    /**
     * Transition to new state after animation ended
     * @param anim The watched animation
     * @param idleState The idle state to which transition is needed
     */
    protected void transitionToIdleIfEndOfAnimation(Animation<TextureRegion> anim, S idleState) {
        if (anim.isAnimationFinished(getStateTimer())) { // on last frame
            setState(idleState, false); // change to default
        }
    }

    /**
     * Returns whether or not the new state should be accepted
     * @param newState The potential new state
     * @return True means yes and False means No
     */
    protected abstract boolean acceptNewState(S newState);

    @Override
    public LazyInit<Body> createBody() {
        return new LazyInit<Body>() {
            @Override
            protected Body createObject() {
                return Behavior.CreateBody(spawnLocation, getWorld(), BodyDef.BodyType.DynamicBody);
            }

            @Override
            protected void onDispose(Body object) {
                super.onDispose(object);
                getWorld().destroyBody(object);
            }
        };
    }

    protected void scheduleForRemoval() {
        getBody().dispose();
        shouldStartRemovalDelay = true;
    }
}
