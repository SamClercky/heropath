package be.samclercky.heropath.sprites.dynamics;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.behavior.KillableBehavior;
import be.samclercky.heropath.behavior.PlayerBehavior;
import be.samclercky.heropath.behavior.ShootingBehavior;
import be.samclercky.heropath.screens.PlayScreen;
import be.samclercky.heropath.sprites.kinematics.PlayerBulletObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class PlayerObject
        extends DynamicObject<PlayerObject.State>
        implements PlayerBehavior.PlayerListener,
        KillableBehavior.KillableListener,
        ShootingBehavior.ShootingBehaviorListener {

    private static final float hitboxWidth = 5 / Constants.PPM;
    private static final float hitboxHeight = 8 / Constants.PPM;

    private final TextureRegion playerRegion;

    public enum State{FALLING, JUMPING, STANDING, RUNNING, DEAD, HIT, ATTACK}

    private Animation<TextureRegion> playerRun;
    private Animation<TextureRegion> playerJump;
    private Animation<TextureRegion> playerIdle;
    private Animation<TextureRegion> playerDead;
    private Animation<TextureRegion> playerHit;
    private Animation<TextureRegion> playerAttack;

    private final PlayerBehavior playerBehavior;
    private final KillableBehavior killableBehavior;
    private ShootingBehavior shootingBehavior;

    private final Sound dropletSound;
    private final Sound hammerSwingSound;
    private final Sound groundSmashingSound;
    private final Sound hitmarkerSound;

    private final PlayScreen screen;

    public PlayerObject(World world, PlayScreen screen) {
        super(
                new Rectangle(0, 0, 40 / Constants.PPM, 40 / Constants.PPM),
                world,
                screen.getKingAtlas().findRegion("Run (78x58)"),
                Constants.ENABLE_DEBUG_SPAWN ? Constants.DEBUG_SPAWN : Constants.SPAWN_LOCATION
        );

        playerBehavior = new PlayerBehavior(
                this
        );

        killableBehavior = new KillableBehavior(
                this,
                3
        );

        try {
            shootingBehavior = new ShootingBehavior(
                    this,
                    ShootingBehavior.ShootType.LINE,
                    5f,
                    3,
                    1,
                    PlayerBulletObject.class
            );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        playerRegion = new TextureRegion(getTexture(), 1,1, 62,58);
        this.screen = screen;

        // Set initial region
        setState(State.STANDING);
        setRegion(playerRegion);

        dropletSound = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/Waterdrop.mp3"));
        hammerSwingSound = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/Woosh.mp3"));
        groundSmashingSound = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/Ground_Smashing.mp3"));
        hitmarkerSound = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/Hitmarker.mp3"));
    }

    /**
     * Get the current hit box
     * @return Current hit box
     */
    @Override
    public Rectangle getHitBox() {
        return new Rectangle(
                0,0,
                hitboxWidth, hitboxHeight
        );
    }

    /**
     * On update after every frame
     * @param delta The time between 2 frames
     */
    @Override
    public void onUpdate(float delta) {
        if (!shouldStartRemovalDelay) {
            playerBehavior.onUpdate(delta);
            killableBehavior.onUpdate(delta);
        }

        shootingBehavior.onUpdate(delta);

        if (!shouldStartRemovalDelay &&
                getCurrentState() == State.DEAD &&
                (playerBehavior != null &&
                        (playerBehavior.isOnGround() ||
                                playerBehavior.isPlayerInWater()))) {
            scheduleForRemoval();
        }

        super.onUpdate(delta);
    }

    /**
     * Draw the bullets associated with the player
     * @param batch Spritebatch
     */
    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        shootingBehavior.drawBullets((SpriteBatch) batch);
    }

    /**
     * Needed to handle special collision detection
     * @return Handler to playerBehavior
     */
    public KillableBehavior getBehaviorHandler() {
        return killableBehavior;
    }

    @Override
    protected void setupAnimation() {
        final Vector2 dimensions = new Vector2(78, 58);

        playerRun = generateAnimation(
                8,
                new Vector2(1, 1),
                dimensions,
                true
        );

        playerJump = generateAnimation(
                1,
                new Vector2(707, 1),
                dimensions,
                false
        );

        playerIdle = generateAnimation(
                11,
                new Vector2(1, 181),
                dimensions,
                true
        );

        playerDead = generateAnimation(
                4,
                new Vector2(627, 61),
                dimensions,
                false
        );

        playerHit = generateAnimation(
                2,
                new Vector2(861, 181),
                dimensions,
                false
        );

        playerAttack = generateAnimation(
                3,
                new Vector2(627, 121),
                dimensions,
                false
        );
    }

    /**
     * Gets the frame that needs to be rendered
     * @param delta Time passed since last call
     * @return The frame that needs rendering
     */
    @Override
    protected TextureRegion getFrame(float delta){
        final boolean isFlip = false;

        switch (getCurrentState()){
            case JUMPING:
                return getFrame(playerJump, isFlip);
            case RUNNING:
                return getFrame(playerRun, isFlip);
            case FALLING:
                return getFrame(playerRegion, isFlip);
            case DEAD:
                return getFrame(playerDead, isFlip);
            case HIT:
                TextureRegion f1 = getFrame(playerHit, isFlip); // Order is important
                transitionToIdleIfEndOfAnimation(playerHit, State.STANDING);
                return f1; // Order is important
            case ATTACK:
                TextureRegion f2 = getFrame(playerAttack, isFlip); // Order is important
                transitionToIdleIfEndOfAnimation(playerAttack, State.STANDING);
                return f2; // Order is important
            case STANDING:
            default:
                return getFrame(playerIdle, isFlip);
        }
    }

    @Override
    protected boolean acceptNewState(State newState) {
        State state = getCurrentState();
        return state != State.HIT && state != State.DEAD && state != State.ATTACK;
    }

    @Override
    public void onDead() {
        setState(State.DEAD);
    }

    @Override
    public void onHit() {
        setState(State.HIT);
    }

    /**
     * One time shots
     * @param newState The new state
     */
    @Override
    protected void onStateChanged(State newState) {
        super.onStateChanged(newState);

        switch (newState) {
            case HIT:
                long idHit = hitmarkerSound.play();
                hitmarkerSound.setPitch(idHit, 4f);
                hitmarkerSound.setVolume(idHit,0.8f);
                hitmarkerSound.setLooping(idHit, false);
                break;
            case ATTACK:
                // Play sound
                long idSwing = hammerSwingSound.play();
                dropletSound.setPitch(idSwing, 1f);
                dropletSound.setVolume(idSwing,0.3f);
                dropletSound.setLooping(idSwing, false);
                break; // to bad we can't use jdk14 :(
            case DEAD:
                if (playerBehavior != null) {
                    playerBehavior.onPlayerDead(); // handle last movements before dieing
                }
                break;
            default:
                // nothing to do
                break;
        }
    }

    @Override
    public void onHitWater() {
        // Play sound
        long idDrop = dropletSound.play();
        dropletSound.setPitch(idDrop, 1f);
        dropletSound.setLooping(idDrop, false);
    }

    @Override
    public PlayScreen getScreen() {
        return screen;
    }

    /**
     * Returns shooting direction
     * @return The direction in which to shoot
     */
    @Override
    public ShootingBehavior.FacingDirection getFacing() {
        if (isFacingLeft())
            return ShootingBehavior.FacingDirection.LEFT;
        else
            return ShootingBehavior.FacingDirection.RIGHT;
    }

    @Override
    public void onShoot() {
        if (!shootingBehavior.shoot()) {
            screen.onNotEnoughStamina();
        } else {
            long idGround = groundSmashingSound.play();
            groundSmashingSound.setPitch(idGround,1f);
            groundSmashingSound.setVolume(idGround, 0.5f);
            groundSmashingSound.setLooping(idGround, false);
        }
    }

    @Override
    public void onPlayerHit(int damage) {
        killableBehavior.onHit(damage);
    }

    @Override
    public void dispose() {
        super.dispose();
        shootingBehavior.dispose();
    }
}
