package be.samclercky.heropath.sprites.dynamics.enemies;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.behavior.ShootingBehavior;
import be.samclercky.heropath.screens.PlayScreen;
import be.samclercky.heropath.sprites.kinematics.EnemyFireBulletObject;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class FirePigObject extends EnemyObject<FirePigObject.State> implements ShootingBehavior.ShootingBehaviorListener {

    public enum State { STANDING, RUNNING, ATTACK, DIED, HIT, IDLE, JUMPING } // TODO: review necessity

    private static final Rectangle hitBox = new Rectangle(0, 0, 10/Constants.PPM, 10/Constants.PPM);

    private static final int INITIAL_LIVES = 3;
    private static final float LINEAR_SPEED = 100 / Constants.PPM;

    private ShootingBehavior shootingBehavior;
    private final PlayScreen screen;

    private TextureRegion firePigRegion;

    private Animation<TextureRegion> firePigRun;
    private Animation<TextureRegion> firePigAttack;
    private Animation<TextureRegion> firePigDead;
    private Animation<TextureRegion> firePigHit;
    private Animation<TextureRegion> firePigIdle;
    private Animation<TextureRegion> firePigJump;

    /**
     * Constructor
     * @param world TMX world
     * @param screen Screen
     * @param spawnLocation spawnLocation of the enemy object given by the TMX files
     */
    public FirePigObject(World world, PlayScreen screen, Vector2 spawnLocation) {
        super(
                new Rectangle(0, 0, 34/Constants.PPM, 28/Constants.PPM),
                world,
                screen.getPigKingAtlas().findRegion("Run (38x28)"),
                spawnLocation,
                LINEAR_SPEED,
                INITIAL_LIVES
        );

        this.screen = screen;

        firePigRegion = new TextureRegion(getTexture(), 1,1,38,28);
        setState(State.RUNNING);
        setRegion(firePigRegion);

        try {
            shootingBehavior = new ShootingBehavior(
                    this,
                    ShootingBehavior.ShootType.CIRCLE,
                    10,
                    3,
                    3f,
                    EnemyFireBulletObject.class
            );
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }

    }

    /**
     * getter for hitbox
     * @return hitbox
     */
    @Override
    public Rectangle getHitBox() {
        return hitBox;
    }

    /**
     * Set up for all the firepig animations
     */
    @Override
    protected void setupAnimation() {
        final Vector2 dimensions = new Vector2(38, 28);

        firePigRun = generateAnimation(
                6,
                new Vector2(1, 1),
                dimensions,
                true
        );

        firePigAttack = generateAnimation(
                5,
                new Vector2(459, 31),
                dimensions,
                false
        );

        firePigDead = generateAnimation(
                4,
                new Vector2(231, 1),
                dimensions,
                false
        );

        firePigHit = generateAnimation(
                2,
                new Vector2(651, 31),
                dimensions,
                false
        );

        firePigIdle = generateAnimation(
                11,
                new Vector2(1, 31),
                dimensions,
                true
        );

        firePigJump = generateAnimation(
                1,
                new Vector2(425, 1),
                dimensions,
                false
        );
    }

    /**
     * get frame of the animation
     * @param delta Time passed since last call
     * @return region
     */
    @Override
    protected TextureRegion getFrame(float delta) {
        final boolean isFlip = true;

        switch(getCurrentState()){
            case HIT:
                TextureRegion f1 = getFrame(firePigHit, isFlip); // Order important
                transitionToIdleIfEndOfAnimation(firePigHit, State.RUNNING);
                return f1; // Order important
            case DIED:
                removeOnLastFrame(firePigDead);
                return getFrame(firePigDead, isFlip);
            case IDLE:
                return getFrame(firePigIdle, isFlip);
            case ATTACK:
                return getFrame(firePigAttack, isFlip);
            case JUMPING:
                return getFrame(firePigJump, isFlip);
            case RUNNING:
                return getFrame(firePigRun, isFlip);
            case STANDING:
            default:
                return getFrame(firePigRegion, isFlip);
        }
    }

    /**
     * The previous animation needs to be finished, before accepting a new state // bug fix
     * @param newState The potential new state
     * @return True is new state is accepted
     */
    @Override
    protected boolean acceptNewState(State newState) {
        State state = getCurrentState();
        return state != State.HIT && state != State.DIED;
    }

    /**
     * set off die logic
     */
    @Override
    public void onDead() {
        setState(State.DIED);
    }

    /**
     * set off hit logic
     */
    @Override
    public void onHit() {
        setState(State.HIT);
    }

    /**
     * Update all positional elements before rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        super.onUpdate(delta);

        shootingBehavior.onUpdate(delta);
    }

    /**
     * draw object on PlayScreen
     * @param batch SpriteBatch
     */
    @Override
    public void draw(Batch batch) {
        super.draw(batch);

        shootingBehavior.drawBullets((SpriteBatch) batch);
    }

    /**
     * Getter for Screen
     * @return screen
     */
    @Override
    public PlayScreen getScreen() {
        return screen;
    }

    /**
     * This method must be called to prevent memory leaks
     */
    @Override
    public void dispose() {
        super.dispose();
        shootingBehavior.dispose();
    }
}
