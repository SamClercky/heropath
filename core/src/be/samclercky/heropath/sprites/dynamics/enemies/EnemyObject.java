package be.samclercky.heropath.sprites.dynamics.enemies;

import be.samclercky.heropath.behavior.EnemyBehavior;
import be.samclercky.heropath.behavior.KillableBehavior;
import be.samclercky.heropath.sprites.dynamics.DynamicObject;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Abstract representation of an enemy
 * @param <S> Forward of State to DynamicObject
 */
public abstract class EnemyObject<S> extends DynamicObject<S> implements EnemyBehavior.EnemyListener, KillableBehavior.KillableListener {
    protected final EnemyBehavior enemyBehavior;
    protected final KillableBehavior killableBehavior;

    /**
     * Constructor
     * @param bounds Rectangle bounds of the enemy object
     * @param world TMX world
     * @param textureRegion textures of the enemy object
     * @param spawnLocation spawnLocation of the enemy object given by the TMX files
     * @param speed speed of the enemy object
     * @param initialLives number of lives the enemy object starts with
     */
    public EnemyObject(Rectangle bounds, World world, TextureRegion textureRegion, Vector2 spawnLocation, float speed, int initialLives) {
        super(bounds, world, textureRegion, spawnLocation);

        killableBehavior = new KillableBehavior(this, 1);
        enemyBehavior = new EnemyBehavior(this, speed, killableBehavior);

        enemyBehavior.setSpeed(speed);
        killableBehavior.resetHealthParameters(initialLives);
    }

    /**
     * Update all positional elements before rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        super.onUpdate(delta);

        if (!shouldStartRemovalDelay) {
            enemyBehavior.onUpdate(delta);
            killableBehavior.onUpdate(delta);
        }
    }

    /**
     * When on last frame ==> remove sprite
     * @param anim Animation to wait for
     */
    protected void removeOnLastFrame(Animation<TextureRegion> anim) {
        if (anim.isAnimationFinished(getStateTimer())) {
            scheduleForRemoval(); // Remove on last frame
        }
    }
}
