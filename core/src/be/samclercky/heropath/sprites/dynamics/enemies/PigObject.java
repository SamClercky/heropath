package be.samclercky.heropath.sprites.dynamics.enemies;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.screens.PlayScreen;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class PigObject extends EnemyObject<PigObject.State> {

    private static final float hitBoxWidth = 8 / Constants.PPM;
    private static final float hitBoxHeight = 8 / Constants.PPM;

    private static final int INITIAL_LIVES = 1;

    private static final float LINEAR_SPEED = 100 / Constants.PPM;

    private final TextureRegion pigRegion;

    public enum State { STANDING, RUNNING, ATTACK, DIED, HIT, IDLE, JUMPING }

    private Animation<TextureRegion> pigRun;
    private Animation<TextureRegion> pigAttack;
    private Animation<TextureRegion> pigDead;
    private Animation<TextureRegion> pigHit;
    private Animation<TextureRegion> pigIdle;
    private Animation<TextureRegion> pigJump;

    /**
     * Constructor
     * @param world TMX world
     * @param screen Screen used
     * @param spawnLocation spawnLocation of the enemy object given by the TMX files
     */
    public PigObject(World world, PlayScreen screen, Vector2 spawnLocation) {
        super(
                new Rectangle(0, 0, 34 / Constants.PPM, 28 / Constants.PPM),
                world,
                screen.getPigAtlas().findRegion("Run (34x28)_pig"),
                spawnLocation,
                LINEAR_SPEED,
                INITIAL_LIVES
        );

        pigRegion = new TextureRegion(getTexture(), 1,1, 34,28);

        // Set initial region
        setState(State.RUNNING);
        setRegion(pigRegion);
    }

    /**
     * Getter for the hitbox
     * @return hitbox
     */
    @Override
    public Rectangle getHitBox() {
        return new Rectangle(
                0, 0,
                hitBoxWidth, hitBoxHeight
        );
    }

    /**
     * set up for the pig animations
     */
    @Override
    protected void setupAnimation() {
        final Vector2 dimensions = new Vector2(34, 28);

        pigRun = generateAnimation(
                6,
                new Vector2(1, 1),
                dimensions,
                true
        );

        pigAttack = generateAnimation(
                5,
                new Vector2(377, 31),
                dimensions,
                false
        );

        pigDead = generateAnimation(
                4,
                new Vector2(207, 1),
                dimensions,
                false
        );

        pigHit = generateAnimation(
                2,
                new Vector2(549, 31),
                dimensions,
                false
        );

        pigIdle = generateAnimation(
                11,
                new Vector2(1, 31),
                dimensions,
                true
        );

        pigJump = generateAnimation(
                1,
                new Vector2(381, 1),
                dimensions,
                false
        );

    }

    /**
     * Get frames of the animations
     * @param delta Time passed since last call
     * @return region
     */
    @Override
    protected TextureRegion getFrame(float delta){
        final boolean isFlip = true;

        switch(getCurrentState()){
            case HIT:
                return getFrame(pigHit, isFlip);
            case DIED:
                removeOnLastFrame(pigDead);
                return getFrame(pigDead, isFlip);
            case IDLE:
                return getFrame(pigIdle, isFlip);
            case ATTACK:
                return getFrame(pigAttack, isFlip);
            case JUMPING:
                return getFrame(pigJump, isFlip);
            case RUNNING:
                return getFrame(pigRun, isFlip);
            case STANDING:
            default:
                return getFrame(pigRegion, isFlip);
        }
    }

    /**
     * The previous animation needs to be finished, before accepting a new state // bug fix
     * @param newState The potential new state
     * @return true if the previous animation is finished
     */
    @Override
    protected boolean acceptNewState(State newState) {
        return true;
    }

    /**
     * set off die logic
     */
    @Override
    public void onDead() {
        setState(State.DIED);
    }

    /**
     * set off hit logic
     */
    @Override
    public void onHit() {
        setState(State.HIT);
    }
}
