package be.samclercky.heropath.sprites.kinematics;

import be.samclercky.heropath.behavior.Behavior;
import be.samclercky.heropath.behavior.BulletBehavior;
import be.samclercky.heropath.behavior.KillableBehavior;
import be.samclercky.heropath.sprites.MovingObject;
import be.samclercky.heropath.utils.LazyInit;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Defines all bullet like object
 */
public abstract class BulletObject
        extends MovingObject
        implements BulletBehavior.BulletBehaviorListener,
                    KillableBehavior.KillableListener {

    private final BulletBehavior bulletBehavior;
    private final KillableBehavior killableBehavior;

    private final Rectangle hitBox;

    /**
     * General constructor for bullets
     * @param bounds Bounds of the texture (how big is the bullet)
     * @param world World the bullet should spawn in
     * @param region TextureRegion of the bullet
     * @param spawnLocation The location in which the bullet should spawn in
     * @param speed The initial speed of the bullet
     * @param direction The initial direction of the bullet
     * @param isPlayer Is the bullet related with the player (did the player shoot?)
     * @param isBouncy Does the bullet bounce when hitting the ground
     * @param hitBox The hit box of the bullet
     */
    public BulletObject(Rectangle bounds,
                        World world,
                        TextureRegion region,
                        Vector2 spawnLocation,
                        float speed,
                        Vector2 direction,
                        boolean isPlayer,
                        boolean isBouncy,
                        Rectangle hitBox) {
        super(
                bounds,
                world,
                region,
                spawnLocation
        );

        this.hitBox = hitBox;
        killableBehavior = new KillableBehavior(this, 1);
        bulletBehavior = new BulletBehavior(
                this,
                speed,
                direction,
                isPlayer,
                isBouncy,
                killableBehavior
        );
    }

    /**
     * Handle on update
     * @param delta Time between 2 frames
     */
    @Override
    public void onUpdate(float delta) {
        super.onUpdate(delta);

        killableBehavior.onUpdate(delta);
        bulletBehavior.onUpdate(delta);
    }

    /**
     * Describe how to create a new body with a lazy body
     * @return Lazy getter for body
     */
    @Override
    public LazyInit<Body> createBody() {
        return new LazyInit<Body>() {
            @Override
            protected Body createObject() {
                return Behavior.CreateBody(getSpawnLocation(), getWorld(), BodyDef.BodyType.KinematicBody);
            }

            @Override
            protected void onDispose(Body object) {
                super.onDispose(object);
                getWorld().destroyBody(object);
            }
        };
    }

    /**
     * Accessor to the animation
     * @return The animation of the bullet
     */
    protected abstract Animation<TextureRegion> getTextureRegion();

    @Override
    protected TextureRegion getFrame(float delta) {
        return getFrame(getTextureRegion(), false);
    }

    /**
     * When dead ==> start cleaning
     */
    @Override
    public void onDead() {
        makeReadyToRemove();
    }

    /**
     * Event for when hit ==> die immediately handled by killable behavior
     */
    @Override
    public void onHit() {
        // Nothing to do
    }

    /**
     * Getter for hit box
     * @return Current hit box
     */
    @Override
    public Rectangle getHitBox() {
        return hitBox;
    }
}
