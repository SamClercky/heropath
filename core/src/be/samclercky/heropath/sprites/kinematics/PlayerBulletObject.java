package be.samclercky.heropath.sprites.kinematics;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.screens.PlayScreen;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class PlayerBulletObject extends BulletObject{
    private final static float SPEED = 12000 / Constants.PPM;
    private final static boolean isPlayer = true;
    private final static boolean isBouncy = true;
    private final static Rectangle hitBox = new Rectangle(0, 0, 3/Constants.PPM, 3/Constants.PPM);

    private Animation<TextureRegion> bulletAnimation;

    /**
     * Creates a new PlayerButton and puts it into the world
     * @param world The world in which the object should be put into
     * @param screen The current PlayScreen
     * @param spawnLocation The spawn location of the bullet
     * @param direction The initial direction of the bullet
     */
    public PlayerBulletObject(World world, PlayScreen screen, Vector2 spawnLocation, Vector2 direction) {
        super(
                new Rectangle(0, 0, 34/Constants.PPM, 28/Constants.PPM),
                world,
                screen.getBulletAtlas().findRegion("stone"),
                spawnLocation,
                SPEED,
                direction,
                isPlayer,
                isBouncy,
                hitBox);

        setRegion(new TextureRegion(getTexture(),1,173,270,270));
    }

    /**
     * Get the current texture (convenience)
     * @return The current texture/animation
     */
    @Override
    protected Animation<TextureRegion> getTextureRegion() {
        return bulletAnimation;
    }

    /**
     * Adds all needed frames to the animation
     */
    @Override
    protected void setupAnimation() {
        bulletAnimation = generateAnimation(
                1,
                new Vector2(1, 173),
                new Vector2(270, 270),
                false
        );
    }
}
