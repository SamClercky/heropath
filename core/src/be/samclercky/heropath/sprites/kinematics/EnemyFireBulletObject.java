package be.samclercky.heropath.sprites.kinematics;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.screens.PlayScreen;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class EnemyFireBulletObject extends BulletObject{
    private final static float SPEED = 1500 / Constants.PPM;
    private final static boolean isPlayer = false;
    private final static boolean isBouncy = true;
    private final static Rectangle hitBox = new Rectangle(0, 0, 25/Constants.PPM, 25/Constants.PPM);

    private final TextureRegion bulletRegion;
    private Animation<TextureRegion> bulletAnimation;

    /**
     * Creates a new EnemyFireBullet
     * @param world World in which the bullet should be put into
     * @param screen The current playScreen
     * @param spawnLocation The spawn location of the bullet
     * @param direction The initial direction of the bullet
     */
    public EnemyFireBulletObject(World world, PlayScreen screen, Vector2 spawnLocation, Vector2 direction) {
        super(
                new Rectangle(0, 0, 34/Constants.PPM, 28/Constants.PPM),
                world,
                screen.getBulletAtlas().findRegion("cannonball"),
                spawnLocation,
                SPEED,
                direction,
                isPlayer,
                isBouncy,
                hitBox);

        bulletRegion = new TextureRegion(getTexture(),1,1,160,170);
        setRegion(bulletRegion);
    }

    /**
     * Get the current texture
     * @return Current texture / animation
     */
    @Override
    protected Animation<TextureRegion> getTextureRegion() {
        return bulletAnimation;
    }

    /**
     * Prepares all the frames for the animation
     */
    @Override
    protected void setupAnimation() {
        bulletAnimation = generateAnimation(
                1,
                new Vector2(1, 1),
                new Vector2(160, 170),
                false
        );
    }
}
