package be.samclercky.heropath.sprites.statics;

import be.samclercky.heropath.behavior.Behavior;
import be.samclercky.heropath.behavior.GroundBehavior;
import be.samclercky.heropath.screens.PlayScreen;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * General structure for all static things
 */
public class WallObject implements GroundBehavior.GroundListener {

    private final World world;
    private final Vector2 spawnLocation;
    private final PlayScreen screen;

    /**
     * Constructor for Polygons
     * @param screen The current playScreen
     * @param world The world in which the sprite should be put in
     * @param geometry The geometry of the polygon
     * @param isGround Is the object ground (needed for some optimizations)
     * @param isFinish Is the object the finishline af the level
     */
    public WallObject(PlayScreen screen, World world, Polygon geometry, boolean isGround, boolean isFinish) {
        this(
                world,
                new Vector2(geometry.getX(), geometry.getY()),
                screen
        );

        new GroundBehavior(this, getRudimentaryBody(), geometry, isGround, isFinish);
    }

    /**
     * Constructor for Rectangles
     * @param screen The current playScreen
     * @param world The world in which the sprite should be put in
     * @param geometry The geometry of the polygon
     * @param isGround Is the object ground (needed for some optimizations)
     * @param isFinish Is the object the finishline af the level
     */
    public WallObject(PlayScreen screen, World world, Rectangle geometry, boolean isGround, boolean isFinish) {
        this(
                world,
                new Vector2(
                        geometry.getX() + geometry.getWidth()/2,
                        geometry.getY() + geometry.getHeight()/2
                ),
                screen
        );

        new GroundBehavior(this, getRudimentaryBody(), geometry, isGround, isFinish);
    }

    /**
     * General constructor
     * @param world The world in which the sprite should be put in
     * @param spawnLocation The location in which the object should spawn
     * @param screen The current playScreen
     */
    private WallObject(World world, Vector2 spawnLocation, PlayScreen screen) {
        this.world = world;
        this.spawnLocation = spawnLocation;
        this.screen = screen;
    }

    /**
     * Create a new body and put it in the world
     * @return The freshly created body
     */
    private Body getRudimentaryBody() {
        return Behavior.CreateBody(spawnLocation, world, BodyDef.BodyType.StaticBody);
    }

    /**
     * Collision detection
     * Is only called when player finishes (isFinish==true) and forwards the win message
     */
    @Override
    public void onPlayerHit() {
        screen.playerWon();
    }
}
