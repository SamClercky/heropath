package be.samclercky.heropath.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class InputFactory {
    /**
     * This is just a factory and may NOT be called
     * @throws UnsupportedOperationException Exception when called (ALWAYS)
     */
    public InputFactory() {
        throw new UnsupportedOperationException("BtnFactory may not be initialised and can only be used as a factory class");
    }

    /**
     * Create a simple text field
     * @param placeholder The placeholder in the text field
     * @return Freshly created text field
     */
    public static TextField createSimpleTextField(String placeholder) {
        TextField.TextFieldStyle style = new TextField.TextFieldStyle();
        style.fontColor = Color.BLACK;
        style.font = new BitmapFont();

        // set background
        Pixmap pixmap = new Pixmap(1,1, Pixmap.Format.RGB565);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        TextureRegionDrawable textureRegionDrawable = new TextureRegionDrawable(
                new TextureRegion(new Texture(pixmap)));
        style.background = textureRegionDrawable;

        // set cursor
        pixmap = new Pixmap(1, 5, Pixmap.Format.RGB565);
        pixmap.setColor(Color.BLACK);
        pixmap.fill();
        textureRegionDrawable = new TextureRegionDrawable(
                new TextureRegion(new Texture(pixmap)));
        style.cursor = textureRegionDrawable;

        return new TextField(placeholder, style);
    }
}
