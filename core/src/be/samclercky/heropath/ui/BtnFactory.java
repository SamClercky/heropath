package be.samclercky.heropath.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class BtnFactory {
    /**
     * This is just a factory and may NOT be called
     * @throws UnsupportedOperationException Exception when called (ALWAYS)
     */
    public BtnFactory() {
        throw new UnsupportedOperationException("BtnFactory may not be initialised and can only be used as a factory class");
    }

    /**
     * Generate a custom button
     * @param text Text to be added
     * @param pathToAsset The internal path to the asset
     * @param fontColor The color the text will have
     * @return The requested button
     */
    private static TextButton getButton(String text, String pathToAsset, Color fontColor) {
        Skin skin = new Skin();
        Texture texture = new Texture(Gdx.files.internal(pathToAsset));
        skin.add("normal", texture);

        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = new BitmapFont();
        style.fontColor = fontColor;
        style.up = skin.getDrawable("normal");
        style.down = skin.getDrawable("normal");

        return new TextButton(text, style);
    }

    /**
     * Generate a custom MenuButton
     * @param text Text to be added
     * @return The requested button
     */
    public static TextButton createMenuButton(String text) {
        return getButton(text, "Graphics/Photos/btn/button.png", Color.BLACK);
    }

    /**
     * Generate a menu button which is less popping
     * @param text The text for the button
     * @return The freshly created button
     */
    public static TextButton createLessPoppingButton(String text) {
        return getButton(text, "Graphics/Photos/btn/less_pop_btn.png", Color.WHITE);
    }
}
