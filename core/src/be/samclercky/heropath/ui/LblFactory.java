package be.samclercky.heropath.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Factory class to easily create styled labels
 */
public abstract class LblFactory {
    /**
     * This is just a factory and may NOT be called
     * @throws UnsupportedOperationException Exception when called (ALWAYS)
     */
    public LblFactory() {
        throw new UnsupportedOperationException("LblFactory may not be initialised and can only be used as a factory class");
    }

    /**
     * Create label with no content (spacer)
     */
    public static Label createEmpty() {
        return createMenuLabel("");
    }

    /**
     * Label for the Menu
     * @param text Label text
     * @return Freshly created label
     */
    public static Label createMenuLabel(String text) {
        return createNormalLabel(text, Color.RED);
    }

    /**
     * Label which wraps around (better for long texts
     * @param text Label text
     * @return Freshly created label
     */
    public static Label createBlockText(String text) {
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = new BitmapFont();
        style.fontColor = Color.WHITE;

        Label result = new Label(text, style);
        result.setWrap(true);

        return result;
    }

    /**
     * Label for the menu-title
     * @param text label text
     * @return Freshly created label
     */
    public static Label createMenuTitleLabel(String text) {
        return createTitleLabel(text, Color.RED);
    }

    /**
     * Label for GameOverScreen with win message
     * @param text label text
     * @return Freshly created label
     */
    public static Label createGreenWinLabel(String text) {
        return createTitleLabel(text, Color.GREEN);
    }

    /**
     * Creates new title label
     * @param text Label text
     * @param color Label color
     * @return Freshly created label
     */
    private static Label createTitleLabel(String text, Color color) {
        BitmapFont font = new BitmapFont();
        font.getData().setScale(3); // Scale font
        Label.LabelStyle style = new Label.LabelStyle(font, color);
        return new Label(text, style);
    }

    /**
     * Label with general information in the HUD
     * @param text Label text
     * @return Fresly created label
     */
    public static Label createHUDLabel(String text) {
        return createNormalLabel(text, Color.BLACK);
    }

    public static Label createGameOverLabel(String text) {
        return createNormalLabel(text, Color.WHITE);
    }

    public static Label createFunnyMessageLabel(String text) {
        return createNormalLabel(text, Color.YELLOW);
    }

    /**
     * Create a normal label
     * @param text Label text
     * @param color Label color
     * @return Fresly created label
     */
    private static Label createNormalLabel(String text, Color color) {
        Label.LabelStyle font = new Label.LabelStyle(new BitmapFont(), color);
        return new Label(text, font);
    }

    /**
     * Create a new Label specific for the high scores screen
     * @param text Label text
     * @return Fresly created label
     */
    public static Label createHighScoresLabel(String text) {
        return createGameOverLabel(text);
    }
}
