package be.samclercky.heropath.utils;

import be.samclercky.heropath.Constants;
import be.samclercky.httpclient.HttpClient;
import be.samclercky.httpclient.HttpEventListener;
import be.samclercky.httpclient.actions.Action;
import be.samclercky.httpclient.exampleactions.AddAction;
import be.samclercky.httpclient.exampleactions.GetAllAction;
import be.samclercky.httpclient.exampleactions.Score;
import be.samclercky.httpclient.exampleactions.respons.GetAllRespons;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import java.util.Arrays;

/**
 * Manager for all http requests to server and caches the data to minimize stress on the server
 */
public class HttpClientManager implements Disposable {
    private static final int cachedDataCapacity = 10;

    private final HttpClient httpClient;
    private final Array<Score> cachedDataFromServer = new Array<>(cachedDataCapacity);
    private boolean isDataUpToDate = false;

    public HttpClientManager() {
        httpClient = new HttpClient();
    }

    /**
     * Retrieves current high score. To get the latest data, the user must poll this function
     * @return PriorityQueue with all high scores, but null if data is not the latest
     */
    public Array<Score> getHighScores() {
        if (!isDataUpToDate) {
            fetchNewData();
            return null;
        } else if (isInProgress()) {
            return null;
        } else {
            return cachedDataFromServer;
        }
    }

    /**
     * Checks is httpClient is busy (and polls new data)
     * @return true if httpClient is busy
     */
    public boolean isInProgress() {
        return httpClient.isBusy();
    }

    /**
     * Send request to db and update local cache
     * @param score The new score to be added
     */
    public void addNewData(Score score) {
        if (cachedDataFromServer.size >= cachedDataCapacity) {
            cachedDataFromServer.pop(); // make place by removing last element
        }
        cachedDataFromServer.add(score); // add new score to score board
        cachedDataFromServer.sort(); // resort all data

        HttpEventListener listener = new HttpEventListener() {
            // No additional implementation needed
        };

        Action action;
        if (Constants.USE_LDB) {
            // When using Database from out professor
            action = new AddAction_LDB(listener, score);
        } else {
            // When using Google Firebase
            action = new AddAction(listener, score);
        }

        sendTask(action);
    }

    /**
     * Fetch new data coming from db and update local cache
     */
    private void fetchNewData() {
        if (!isInProgress()) {

            HttpEventListener listener = new HttpEventListener() {

                @Override
                public void onRequestFinished(Object result) {
                    isDataUpToDate = true;

                    GetAllRespons data = (GetAllRespons) result;

                    cachedDataFromServer.clear();

                    Score[] values = data.data.values().toArray(new Score[0]);
                    Arrays.sort(values);

                    for (int i = 0; i < 10 && i < values.length; i++) {
                        cachedDataFromServer.add(values[i]);
                    }
                }
            };

            Action action;
            if (Constants.USE_LDB) {
                // When using the database from our professor
                action = new GetAction_LDB(listener);
            } else {
                // When using Google Firebase
                action = new GetAllAction(listener);
            }

            sendTask(action);
        }
    }

    /**
     * Send task to db
     * @param action The action to performed on server
     */
    private void sendTask(Action action) {
        // DEBUGGING
        if (!Constants.DISABLE_HTTP_REQUESTS)
            httpClient.addTask(action);
    }

    /**
     * Close httpClient properly
     */
    @Override
    public void dispose() {
        httpClient.close();
    }
}
