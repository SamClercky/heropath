package be.samclercky.heropath.utils;

public class NotSupportedEnemyClassException extends Exception {
    @Override
    public String toString() {
        return "The given class is not supported as an enemy";
    }
}
