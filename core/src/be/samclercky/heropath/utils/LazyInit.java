package be.samclercky.heropath.utils;

import com.badlogic.gdx.utils.Disposable;

/**
 * Utility for lazy initialisation (like Kotlin's lateinit)
 * @param <T> The wrapped object
 */
public abstract class LazyInit<T> implements Disposable {
    private T object = null;
    private boolean isDisposed = false;

    /**
     * Create the object
     * @return The created object
     */
    protected abstract T createObject();

    /**
     * Optional predicate to manage creation
     * @return true if the object may be created, false to not create a new object
     */
    protected boolean creationPredicate() { return true; }

    /**
     * When object is being cleaned ==> clean object properly
     * @param object The current stored object (is going to be destroyed)
     */
    protected void onDispose(T object) {
        // no default implementation
    }

    public T get() {
        if (!isDisposed && object == null && creationPredicate()) {
            object = createObject();
        }

        return object;
    }

    @Override
    public void dispose() {
        if (!isDisposed) {
            onDispose(object);
            isDisposed = true;
            object = null;
        }
    }
}
