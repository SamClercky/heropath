package be.samclercky.heropath.utils;

import be.samclercky.heropath.HeroPath;
import com.badlogic.gdx.Screen;

public class Utils {

    /**
     * Transitions to new Screen and destroys the current one
     * @param game The global game instance
     * @param current The current Screen
     * @param to The Screen to transition to
     */
    public static void transitionToScreen(HeroPath game, Screen current, Screen to) {
        if (current != null)
            current.dispose();

        game.setScreen(to);
    }
}
