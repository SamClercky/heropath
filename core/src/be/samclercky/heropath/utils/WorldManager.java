package be.samclercky.heropath.utils;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.screens.PlayScreen;
import be.samclercky.heropath.sprites.dynamics.enemies.EnemyObject;
import be.samclercky.heropath.sprites.dynamics.enemies.FirePigObject;
import be.samclercky.heropath.sprites.dynamics.enemies.PigObject;
import be.samclercky.heropath.sprites.statics.WallObject;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Manages ownership of the world variable
 */
public class WorldManager {

    private World world;
    private Box2DDebugRenderer b2dr;

    //Variables for Tiled map.
    private TmxMapLoader mapLoader; //loads map into game
    private TiledMap map; //reference to game
    private OrthogonalTiledMapRenderer renderer; //renders map on screen

    private Array<EnemyObject<?>> enemies;

    public WorldManager(PlayScreen screen) {
        // create world + gravity
        world = new World(new Vector2(0, -1000/Constants.PPM), true);
        b2dr = new Box2DDebugRenderer();

        mapLoader = new TmxMapLoader();
        map = mapLoader.load("Graphics/Tiled_stuff/Level1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / Constants.PPM);

        // Generate bodies
        generateStaticObjectsFromTiledLayer(9, screen,false); // Normal ground
        generateStaticObjectsFromTiledLayer(12, screen, true); // Finish ground

        enemies = new Array<>();

        try {
            generateEnemiesFromLayer(10, screen, PigObject.class);
            generateEnemiesFromLayer(11, screen, FirePigObject.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Add contactListener
        world.setContactListener(new WorldContactListener(screen));
    }

    public void onUpdate(float delta, OrthographicCamera camera) {
        world.step(1/60f, 6, 2);
        renderer.setView(camera);

        for (EnemyObject<?> enemy: enemies) {
            if (enemy.canDispose()) {
                enemy.dispose();
                enemies.removeValue(enemy, true);
            } else {
                enemy.onUpdate(delta);
            }
        }
    }

    public void onRender(OrthographicCamera gameCam) {
        renderer.render();

        // DEBUGGING
        if (Constants.ENABLE_B2DR) {
            b2dr.render(world, gameCam.combined);
        }
    }

    public void onEnemiesDraw(SpriteBatch batch) {
        for (EnemyObject<?> enemy: enemies) {
            enemy.draw(batch);
        }
    }

    public World getWorld() {
        return world;
    }

    private void generateStaticObjectsFromTiledLayer(int index, PlayScreen screen, boolean isFinish) {
        // Polygons
        for (MapObject object : map.getLayers().get(index).getObjects().getByType(PolygonMapObject.class)) {
            Polygon poly = ((PolygonMapObject) object).getPolygon();

            new WallObject(screen, world, poly, true, isFinish);
        }

        // Rectangles
        for (MapObject object : map.getLayers().get(index).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle poly = ((RectangleMapObject) object).getRectangle();

            new WallObject(screen, world, poly, true, isFinish);
        }
    }

    /**
     * Generates all the enemies from a TMX-file and cast them to a given class
     * @param index The index of the layer in the TMX-file
     * @param screen The screen on which to render
     * @param clazz Which enemy the layer represents
     * @throws NotSupportedEnemyClassException Is thrown when the clazz could not be created
     */
    private void generateEnemiesFromLayer(int index, PlayScreen screen, Class<?> clazz) throws NotSupportedEnemyClassException {
        for (MapObject object : map.getLayers().get(index).getObjects().getByType(RectangleMapObject.class)) {

            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            Vector2 spawnLoc = new Vector2(rect.x, rect.y);

            try {
                Constructor<?> enemyConstructor = clazz.getConstructor(World.class, PlayScreen.class, Vector2.class);
                EnemyObject<?> enemy = (EnemyObject<?>) enemyConstructor.newInstance(getWorld(), screen, spawnLoc);

                enemies.add(enemy);
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException ex) {
                throw new NotSupportedEnemyClassException();
            } catch (InvocationTargetException ex) {
                ex.printStackTrace();
            }
        }
    }
}
