package be.samclercky.heropath.utils;

import be.samclercky.httpclient.exampleactions.Score;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Data {
    public List<Item> Scores;

    public Map<String, Score> toScores(){
        Map<String, Score> result = new HashMap<>();

        for (Item scores: Scores){
            Score newScore = new Score();
            newScore.name = scores.Player;
            newScore.score = Integer.parseInt(scores.Result.replaceAll("[^0-9]",""));
            result.put(Integer.toString(scores.hashCode()),newScore);
        }
        return result;
    }


}

class Item{
    public String Result;
    public String Player;
    public String DateCreated;
}
