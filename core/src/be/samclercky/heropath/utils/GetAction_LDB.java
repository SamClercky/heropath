package be.samclercky.heropath.utils;

import be.samclercky.httpclient.HttpEventListener;
import be.samclercky.httpclient.actions.Action;
import be.samclercky.httpclient.actions.ActionTypes;
import be.samclercky.httpclient.exampleactions.respons.GetAllRespons;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class GetAction_LDB implements Action {

    private HttpEventListener listener;

    public GetAction_LDB(HttpEventListener listener){
        this.listener = listener;
    }

    @Override
    public String getURL() {
        return "https://rapptor.vub.ac.be/gamemanager/api/getScores";
    }

    @Override
    public ActionTypes getActionType() {
        return ActionTypes.POST;
    }

    @Override
    public Map<String, Object> getFormData() {
        Map<String, Object> result = new HashMap<>();
        result.put("gameName", "HeroPath");
        result.put("limit", "10");
        return result;
    }

    @Override
    public HttpEventListener getListener() {
        return listener;
    }

    @Override
    public GetAllRespons getDataFromRequestedJSON(String json) {
        return new GetAllRespons(((new Gson()).fromJson(json, Data.class)).toScores());
    }
}
