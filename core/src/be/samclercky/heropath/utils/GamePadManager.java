package be.samclercky.heropath.utils;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;

/**
 * Wrapper class around the controller api but with some conveniences to automate the process
 * When no controller has been found, default behavior will be forwarded
 *
 * All static methods to be as similar as possible to the original api
 */
public class GamePadManager {
    private static Controller controller = null;

    private static final float coolDownTime = 0.1f;
    private static float coolDownTimer = coolDownTime;

    public GamePadManager() {
        // Initialize controllers and pick the first usable one
        for (Controller c: Controllers.getControllers()) {
            if (XBox360Pad.isXBox(c.getName())) { // check if the controller is usable
                controller = c;
                break;
            }
        }
    }

    public static void onUpdate(float delta) {
        coolDownTimer += delta;
    }

    public static boolean getButton(int buttonKey) {
        if (controller == null) return false;

        return controller.getButton(buttonKey);
    }

    /**
     * Same as getButton, but with cool down
     * @param buttonKey The requested key
     * @return Whether or not the key is just pressed
     */
    public static boolean buttonJustPressed(int buttonKey) {
        if (coolDownTimer < coolDownTime) return false;

        if (getButton(buttonKey)) { // delegates to getButton(buttonKey)
            coolDownTimer = 0;
            return true;
        }
        return false;
    }

    public static float getAxis(int axisKey) {
        if (controller == null) return 0;

        return controller.getAxis(axisKey);
    }
}
