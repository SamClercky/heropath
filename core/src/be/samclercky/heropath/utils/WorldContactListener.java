package be.samclercky.heropath.utils;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.behavior.*;
import be.samclercky.heropath.screens.PlayScreen;
import com.badlogic.gdx.physics.box2d.*;

public class WorldContactListener implements ContactListener {
    private final PlayScreen screen;

    public WorldContactListener(PlayScreen screen) {
        this.screen = screen;
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        int collisionDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

        switch (collisionDef) {
            case Constants.ENEMY_BIT | Constants.PLAYER_BIT:
                if (fixA.getFilterData().categoryBits == Constants.PLAYER_BIT)
                    if (fixA.getUserData() == PlayerBehavior.FEET_TAG) {
                        hitEnemy((EnemyBehavior.EnemyCollisionHandler) fixB.getUserData());
                    } else {
                        hitPlayer((PlayerBehavior.PlayerCollisionListener) fixA.getUserData());
                    }
                else
                    if (fixB.getUserData() == PlayerBehavior.FEET_TAG) {
                        hitEnemy((EnemyBehavior.EnemyCollisionHandler) fixA.getUserData());
                    } else {
                        hitPlayer((PlayerBehavior.PlayerCollisionListener) fixB.getUserData());
                    }
                break;
            case Constants.ENEMY_BIT | Constants.WALL_BIT:
                if (fixA.getUserData() instanceof GroundBehavior.GroundCollisionListener
                        && !((GroundBehavior.GroundCollisionListener) fixA.getUserData()).isGround()) {
                    enemyHitWall((EnemyBehavior.EnemyCollisionHandler) fixB.getUserData());
                } else if (fixB.getUserData() instanceof GroundBehavior.GroundCollisionListener
                        && !((GroundBehavior.GroundCollisionListener) fixB.getUserData()).isGround()) {
                    enemyHitWall((EnemyBehavior.EnemyCollisionHandler) fixA.getUserData());
                }
                break;
            case Constants.PLAYER_BULLET_BIT | Constants.PLAYER_BIT:
            case Constants.PLAYER_BULLET_BIT | Constants.ENEMY_BIT:
            case Constants.ENEMY_BULLET_BIT | Constants.PLAYER_BIT:
            case Constants.ENEMY_BULLET_BIT | Constants.ENEMY_BIT:
                boolean withPlayer = (collisionDef & Constants.PLAYER_BIT) != 0; // ==0 zonder player; <> 0 met player

                if (fixA.getUserData() instanceof BulletBehavior.BulletCollisionHandle)
                    if (withPlayer)
                        bulletHit(
                                (BulletBehavior.BulletCollisionHandle) fixA.getUserData(),
                                (PlayerBehavior.PlayerCollisionListener) fixB.getUserData());
                    else
                        bulletHit((BulletBehavior.BulletCollisionHandle) fixA.getUserData(),
                                (EnemyBehavior.EnemyCollisionHandler) fixB.getUserData());
                else
                    if (withPlayer)
                        bulletHit(
                                (BulletBehavior.BulletCollisionHandle) fixB.getUserData(),
                                (PlayerBehavior.PlayerCollisionListener) fixA.getUserData());
                    else
                        bulletHit((BulletBehavior.BulletCollisionHandle) fixB.getUserData(),
                                (EnemyBehavior.EnemyCollisionHandler) fixA.getUserData());
                break;
            case Constants.PLAYER_BIT | Constants.OBJECT_BIT:
                if (fixA.getUserData() instanceof GroundBehavior.GroundCollisionListener)
                    playerHitGround((GroundBehavior.GroundCollisionListener) fixA.getUserData());
                if (fixB.getUserData() instanceof GroundBehavior.GroundCollisionListener)
                    playerHitGround((GroundBehavior.GroundCollisionListener) fixB.getUserData());
                break;
            default:
                break;
        }
    }

    private void hitPlayer(PlayerBehavior.PlayerCollisionListener listener) {
        listener.onHitPlayer(1);
    }

    private void hitEnemy(EnemyBehavior.EnemyCollisionHandler enemy) {
        enemy.onPlayerHit();
        screen.getPlayerHandler().onKillEnemy();
        screen.addScore(200);
    }

    private void enemyHitWall(EnemyBehavior.EnemyCollisionHandler enemy) {
        enemy.onWallHit();
    }

    /**
     * Bullet hit with Player
     * @param handle The bulletHandle
     */
    private void bulletHit(BulletBehavior.BulletCollisionHandle handle, PlayerBehavior.PlayerCollisionListener player) {
        hitPlayer(player);
        bulletHit(handle, true);
    }

    private void bulletHit(BulletBehavior.BulletCollisionHandle handle, EnemyBehavior.EnemyCollisionHandler other) {
        hitEnemy(other);
        bulletHit(handle, false);
    }

    private void bulletHit(BulletBehavior.BulletCollisionHandle handle, boolean withPlayer) {
        if (withPlayer)
            handle.onHitPlayer();
        else
            handle.onHitEnemy();
    }

    private void playerHitGround(GroundBehavior.GroundCollisionListener listener) {
            listener.onPlayerHit();
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
