package be.samclercky.heropath.utils;

import be.samclercky.httpclient.HttpEventListener;
import be.samclercky.httpclient.actions.Action;
import be.samclercky.httpclient.actions.ActionTypes;
import be.samclercky.httpclient.exampleactions.Score;

import java.util.HashMap;
import java.util.Map;

public class AddAction_LDB implements Action {
    private HttpEventListener listener;
    private Score score;

    public AddAction_LDB(HttpEventListener listener, Score score) {
        this.listener = listener;
        this.score = score;
    }

    @Override
    public String getURL() {
        return "https://rapptor.vub.ac.be/gamemanager/api/addScoreJava";
    }

    @Override
    public ActionTypes getActionType() {
        return ActionTypes.POST;
    }

    @Override
    public Map<String, Object> getFormData() {
        Map<String, Object> result = new HashMap<>();
        result.put("player", score.name);
        result.put("result", Integer.toString(score.score));
        result.put("secCode", "457200");
        result.put("gameName", "HeroPath");

        return result;
    }

    @Override
    public HttpEventListener getListener() {
        return listener;
    }

    @Override
    public String getDataFromRequestedJSON(String json) {
        return json;
    }
}
