package be.samclercky.heropath.overlay;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.screens.MenuScreen;
import be.samclercky.heropath.screens.PlayScreen;
import be.samclercky.heropath.ui.BtnFactory;
import be.samclercky.heropath.ui.LblFactory;
import be.samclercky.heropath.utils.Utils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Hud implements Disposable {

    public Stage stage;
    private Viewport viewport; //If your player moves, the HUD moves with the player

    private Integer worldTimer;
    private float timeCount;
    private Integer score;

    private final Label countdownDataLabel;
    private final Label scoreDataLabel;
    private final Label levelDataLabel;
    private final Label livesDataLabel;
    private final Label staminaLabel;

    private final Image pause;
    private final TextButton menuButton;

    private final PlayScreen screen;

    private float staminaVisisble = 3;
    private float staminaVisibleTimer = staminaVisisble;

    /**
     * Constructor
     * @param sb SpriteBatch
     * @param screen PlayScreen
     */
    public Hud(SpriteBatch sb, PlayScreen screen){
        //define HUD variables
        worldTimer = 300;
        timeCount = 0;
        score = 0;

        //define screen
        this.screen = screen;

        //define viewport
        viewport = new FitViewport(Constants.V_WIDTH, Constants.V_HEIGHT, new OrthographicCamera());

        //define stage
        stage = new Stage(viewport, sb);

        //define table
        Table table = new Table();
        table.top();
        table.setFillParent(true);

        //Define labels
        Label timeLabel = LblFactory.createHUDLabel("TIME");
        Label worldLabel = LblFactory.createHUDLabel("WORLD");
        Label heroPathLabel = LblFactory.createHUDLabel("SCORE");
        Label livesLabel = LblFactory.createHUDLabel("LIVES");

        //create labels
        countdownDataLabel = LblFactory.createHUDLabel(String.format("%03d", worldTimer));
        scoreDataLabel = LblFactory.createHUDLabel(String.format("%06d", score));
        levelDataLabel = LblFactory.createHUDLabel("LEVEL 1");
        livesDataLabel = LblFactory.createHUDLabel(String.format("%d",getPlayerHealth()));
        staminaLabel = LblFactory.createMenuLabel("! Not enough stamina !");
        staminaLabel.setFillParent(true);
        staminaLabel.setAlignment(Align.center);
        staminaLabel.setVisible(false);

        //pause button
        Texture texture_pause = new Texture("Graphics/Photos/btn/pause_button.png");
        pause = new Image(texture_pause);
        pause.setVisible(false);

        Stack midSection = new Stack();
        midSection.add(staminaLabel);
        midSection.add(pause);

        // Back to menu
        menuButton = BtnFactory.createMenuButton("MENU");
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Utils.transitionToScreen(screen.getGame(), screen, new MenuScreen(screen.getGame()));
                super.clicked(event, x, y);
            }
        });
        menuButton.setVisible(false);

        //add labels to table
        table.add(heroPathLabel).expandX().padTop(10);
        table.add(worldLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.add(livesLabel).expandX().padTop(10);
        table.row();
        table.add(scoreDataLabel).expandX();
        table.add(levelDataLabel).expandX();
        table.add(countdownDataLabel).expandX();
        table.add(livesDataLabel).expandX();
        table.row();
        table.add(midSection).colspan(4).expandX().center();
        table.row();
        table.add(menuButton).colspan(4).expandX().center().padBottom(10);

        //add table to stage
        stage.addActor(table);

        //debug mode for tables
        if (Constants.ENABLE_UI_DEBUG == true) {
            table.setDebug(true);
        }
    }

    /**
     * Show the timer for stamina reboot
     */
    public void onShowStamina() {
        staminaVisibleTimer = 0;
    }

    /**
     * Called when the screen should render itself.
     * @param dt The time in seconds since the last render.
     * @param isPause if the game is paused, the HUD should also be paused
     */
    public void update(float dt, boolean isPause){
        staminaVisibleTimer += dt;

        if (staminaVisibleTimer < staminaVisisble) {
            staminaLabel.setVisible(true);
        } else {
            staminaLabel.setVisible(false);
        }

        //if the player is dead and the game is paused, the timer should pause
        if (!screen.getPlayerHandler().isDead() && !isPause) {

            // Update timer
            timeCount += dt;
            if (worldTimer == 0){
                screen.getPlayerHandler().onHit(getPlayerHealth()+1);
            }
            if (timeCount >= 1){
                worldTimer=Math.max(0, worldTimer-1);
                countdownDataLabel.setText(getWorldTimerString());
                timeCount = 0;
            }
        }

        // Update lifes
        livesDataLabel.setText(getPlayerHealth());

        pause.setVisible(isPause);
        menuButton.setVisible(isPause);
    }

    /**
     * Add a certain value to the score
     * @param value of how much should be added to the score
     */
    public void addScore(int value){
        score += value;
        scoreDataLabel.setText(String.format("%06d", score));
    }

    /**
     * Accessor for the score
     * @return  score
     */
    public int getScore() {
        return score;
    }

    /**
     * If you complete a level, you receive a bonus based on the time and lives left.
     */
    public void addWinScore(){
        score += worldTimer*3 + getPlayerHealth()*1000;
    }

    /**
     * Accessor player health in a int
     * @return String of player health
     */
    private int getPlayerHealth() {
        if (screen.getPlayerHandler() == null) return 0;
        return screen.getPlayerHandler().getHealth();
    }

    /**
     * Accessor timer in a String
     * @return String of the timer
     */
    private String getWorldTimerString() {
        return String.format("%03d", worldTimer);
    }

    public void acquireInputProcessor() {
        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Called when the Application is destroyed.
     */
    @Override
    public void dispose() {
        stage.dispose();
    }
}
