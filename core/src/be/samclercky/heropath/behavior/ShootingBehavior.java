package be.samclercky.heropath.behavior;


import be.samclercky.heropath.Constants;
import be.samclercky.heropath.screens.PlayScreen;
import be.samclercky.heropath.sprites.kinematics.BulletObject;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ShootingBehavior extends Behavior<ShootingBehavior.ShootingBehaviorListener> implements Disposable {

    public enum ShootType { CIRCLE, LINE }

    public enum FacingDirection { LEFT, UP, RIGHT }

    /**
     * Communication with on screen graphics
     */
    public interface ShootingBehaviorListener {
        World getWorld();
        PlayScreen getScreen();
        Vector2 getCurrentPosition();

        /**
         * Optional method when ShootType.LINE has been selected otherwise neglected
         * @return The direction in which the bullets should shoot
         */
        default FacingDirection getFacing() {
            throw new IllegalStateException("Get facing must be implemented if type of shot is line");
        }
    }

    private float stateTimer = 0;
    private final int maxBulletsOnScreen;
    private final Array<BulletObject> bulletContainer;
    private final ShootType type;
    private final int bulletsPerShot;
    private final boolean isAutomatic;
    private final float shootingPeriod;
    private final Constructor<?> bulletObjectConstructor;
    private final float maxStamina;
    private float currStamina;

    /**
     * Logic of shooting from a object | Master Constructor
     * @param interactiveObject Reference to the visual representation of the object on screen.
     * @param type Shape of the shot. if type == line --> getFacing(); must be implemented
     * @param maxStamina How much stamina does the object have? (0 is infinite)
     * @param maxBulletsOnScreen How many bullets are allowed on screen from a object at the same time?
     * @param bulletsPerShot How many bullets per shot
     * @param isAutomatic Does the behavior has to shoot automaticaly?
     * @param shootingPeriod if isAutomatic == true --> delay betweem 2 shots
     * @param bulletType What kind of bullet are you shooting?
     * @throws NoSuchMethodException Thrown when bullet type is not compatible
     */
    private ShootingBehavior(
            ShootingBehaviorListener interactiveObject,
            ShootType type,
            float maxStamina,
            int maxBulletsOnScreen,
            int bulletsPerShot,
            boolean isAutomatic,
            float shootingPeriod,
            @SuppressWarnings("rawtypes") Class bulletType
    ) throws NoSuchMethodException {
        super(interactiveObject);

        this.type = type;
        this.maxStamina = maxStamina;
        this.currStamina = maxStamina;
        this.maxBulletsOnScreen = maxBulletsOnScreen;
        this.bulletsPerShot = bulletsPerShot;
        this.isAutomatic = isAutomatic;
        this.shootingPeriod = shootingPeriod;

        this.bulletObjectConstructor = bulletType.getConstructor(
                World.class, // World world
                PlayScreen.class, // PlayScreen screen
                Vector2.class, // Vector2 spawnLocation
                Vector2.class // Vector2 direction
        );

        bulletContainer = new Array<>();

        // Sanitize incoming
        assert maxStamina > 0;
        assert bulletsPerShot > 0;
        assert maxBulletsOnScreen > 0;
        assert maxBulletsOnScreen >= bulletsPerShot;
        assert shootingPeriod >= 0;
    }

    /**
     * Logic of shooting from a object | Constructor | Automatic shooting
     * @param interactiveObject Reference to the visual representation of the object on screen.
     * @param type Shape of the shot. if type == line --> getFacing(); must be implemented
     * @param maxBulletsOnScreen How many bullets are allowed on screen from a object at the same time?
     * @param bulletsPerShot How many bullets per shot
     * @param shootingPeriod How many bullets per shot
     * @param bulletType What kind of bullet are you shooting?
     * @throws NoSuchMethodException Thrown when bullet type is not compatible
     */
    public ShootingBehavior(
            ShootingBehaviorListener interactiveObject,
            ShootType type,
            int maxBulletsOnScreen,
            int bulletsPerShot,
            float shootingPeriod,
            @SuppressWarnings("rawtypes") Class bulletType
    ) throws NoSuchMethodException {
        this(interactiveObject, type, 0, maxBulletsOnScreen, bulletsPerShot, true, shootingPeriod, bulletType);
    }

    /**
     * Logic of shooting from a object | Constructor | Manual shooting
     * @param interactiveObject Reference to the visual representation of the object on screen.
     * @param type Shape of the shot. if type == line --> getFacing(); must be implemented
     * @param maxStamina How much stamina does the object have? (0 is infinite)
     * @param maxBulletsOnScreen How many bullets are allowed on screen from a object at the same time?
     * @param bulletsPerShot How many bullets per shot
     * @param bulletType  What kind of bullet are you shooting?
     * @throws NoSuchMethodException Thrown when bullet type is not compatible
     */
    public ShootingBehavior(
            ShootingBehaviorListener interactiveObject,
            ShootType type,
            float maxStamina,
            int maxBulletsOnScreen,
            int bulletsPerShot,
            @SuppressWarnings("rawtypes") Class bulletType
    ) throws NoSuchMethodException {
        this(interactiveObject, type, maxStamina, maxBulletsOnScreen, bulletsPerShot, false, 0, bulletType);
    }

    /**
     * Shoots bullets following the given rules.
     * If isAutomatic == true ==> the user should not call this function on his own behalf
     * @return True is bullet was spawn, False is not
     */
    public boolean shoot() {
        if (currStamina < maxStamina) {
            return false; // Not enough stamina
        }

        // withdraw stamina
        currStamina = 0;

        for (int i = 0; i < bulletsPerShot; i++) {
            BulletObject newBullet;

            switch (type) {
                case CIRCLE:
                    double rad = ((double)i/(double) (bulletsPerShot-1)) * Math.PI/2 + Math.PI/4;

                    newBullet = createBullet(new Vector2(
                            (float) Math.cos(rad),
                            (float) Math.sin(rad)
                    ));
                    break;
                case LINE:
                    switch (interactiveObject.getFacing()) {
                        case LEFT:
                            newBullet = createBullet(new Vector2(
                                    -1, 0
                            ));
                            break;
                        case UP:
                            newBullet = createBullet(new Vector2(
                                    0, 1
                            ));
                            break;
                        case RIGHT:
                            newBullet = createBullet(new Vector2(
                                    1, 0
                            ));
                            break;
                        default:
                            throw new IllegalStateException("Unsupported state: " + interactiveObject.getFacing());
                    }
                    break;
                default:
                    throw new IllegalStateException("Unsupported state: " + type);
            }

            if (newBullet != null) {
                if (bulletContainer.size >= maxBulletsOnScreen) {
                    bulletContainer.first().dispose();
                    bulletContainer.removeIndex(0);
                }
                bulletContainer.add(newBullet);
            }
        }

        return true;
    }

    /**
     * Passes the batch on to all visible bullets
     * @param batch The SpriteBatch
     */
    public void drawBullets(SpriteBatch batch) {
        for (BulletObject bullet: bulletContainer) {
            bullet.draw(batch);
        }
    }

    /**
     * Generate new bullet given a specified direction
     * @param direction The direction of the bullet
     * @return The freshly created bullet or null if internal exception
     */
    private BulletObject createBullet(Vector2 direction) {
        Vector2 currPos = interactiveObject.getCurrentPosition();
        Vector2 spawnLoc = new Vector2( // Needs up scaling
                currPos.x * Constants.PPM,
                currPos.y * Constants.PPM
        );

        try {
            return (BulletObject) bulletObjectConstructor
                    .newInstance(
                            interactiveObject.getWorld(),
                            interactiveObject.getScreen(),
                            spawnLoc,
                            direction
                    );
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Delete the bullet
     * @param bullet in question
     */
    private void deleteBullet(BulletObject bullet) {
        bullet.dispose();
        bulletContainer.removeValue(bullet, true);
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        currStamina += delta;

        if (isAutomatic) {
            stateTimer += delta;

            if (stateTimer - shootingPeriod > 0) {
                shoot();
                stateTimer = 0;
            }
        }

        // clean
        for (BulletObject bullet: bulletContainer) {
            bullet.onUpdate(delta);
            if (bullet.canDispose())
                deleteBullet(bullet);
        }
    }

    /**
     * This method must be called to prevent memory leaks
     */
    @Override
    public void dispose() {
        for (BulletObject bullet: bulletContainer) {
            deleteBullet(bullet);
        }
    }
}
