package be.samclercky.heropath.behavior;

import be.samclercky.heropath.utils.LazyInit;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;

public class MovingBehavior extends Behavior<MovingBehavior.MovingListener> {

    /**
     * Listener for movement of an objects
     */
    public interface MovingListener {
        Rectangle getBounds();

        LazyInit<Body> getBody();

        /**
         * Fires when a new position is requested
         * @param x The new x-coordinate
         * @param y The new y-coordinate
         */
        void onShouldResetPosition(float x, float y);
    }

    /**
     * Constructor
     * @param interactiveObject Visual representation of the object on screen.
     */
    public MovingBehavior(MovingListener interactiveObject) {
        super(interactiveObject);
    }

    /**
     * Method that updates coördinates of the body
     * @param body Box2D body
     */
    private void fireShouldUpdatePosition(Body body) {
        interactiveObject.onShouldResetPosition(
                body.getPosition().x - interactiveObject.getBounds().width / 2,
                body.getPosition().y - interactiveObject.getBounds().height / 2);
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        //Body body = interactiveObject.getBody();
        if (interactiveObject.getBody().get() != null) {
            fireShouldUpdatePosition(interactiveObject.getBody().get());
        }
    }

}
