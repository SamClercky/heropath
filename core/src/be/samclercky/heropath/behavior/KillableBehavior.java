package be.samclercky.heropath.behavior;

import com.badlogic.gdx.math.Vector2;

public class KillableBehavior extends Behavior<KillableBehavior.KillableListener> {

    /**
     * Listener for killable behavior
     */
    public interface KillableListener {
        void onDead();

        // Needed for general availability in Player and Pig objects
        void onHit();

        Vector2 getCurrentPosition();
    }

    private int health = 1;
    private int maxHealth = 1;

    private static final float damageTimeOut = 0.1f; // Timeout so there is no 'dubble' damage
    private float timeSinceLastDamage = damageTimeOut; // From first frame, you can be killed

    /**
     * Constructor
     * @param interactiveObject Visual representation of the object on screen.
     * @param health how much health a object has
     */
    public KillableBehavior(KillableListener interactiveObject, int health) {
        super(interactiveObject);

        resetHealthParameters(health);
    }

    /**
     * Resets health
     * @param health how much health a object has
     */
    public void resetHealthParameters(int health) {
        this.health = health;
        this.maxHealth = health;
    }

    /**
     * Event handler: onHit is when you get hit by other entity
     * @param damage damage dealt
     */
    public void onHit(int damage) {
        if (timeSinceLastDamage >= damageTimeOut) {
            health = Math.max(0, health - damage);
            interactiveObject.onHit();

            // reset timer
            timeSinceLastDamage = 0;
        }
    }

    /**
     * Make temporary invulnerable when hitting enemy
     */
    public void onKillEnemy() {
        timeSinceLastDamage = 0;
    }

    /**
     * Healing logic
     * @param health how much health should be added
     */
    public void onHealing(int health) {
        this.health = Math.min(this.health + health, maxHealth);
    }

    /**
     * Checks if an object is dead
     * @return boolean if dead
     */
    public boolean isDead() {
        return health <= 0 || interactiveObject.getCurrentPosition().y < -1;
    }

    /**
     * Getter for health of an object
     * @return health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        if (isDead())
            interactiveObject.onDead();

        timeSinceLastDamage += delta;
    }

}
