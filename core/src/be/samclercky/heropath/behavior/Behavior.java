package be.samclercky.heropath.behavior;

import be.samclercky.heropath.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public abstract class Behavior<T> {
    /**
     * Reference to the visual representation of the object on screen.
     */
    protected T interactiveObject;

    /**
     * Constructor
     * @param interactiveObject Reference to the visual representation of the object on screen.
     */
    public Behavior(T interactiveObject) {
        this.interactiveObject = interactiveObject;
    }

    /**
     * Interface method on screen rerender
     * @param delta The time in seconds since the last render.
     */
    public abstract void onUpdate(float delta);

    /**
     * Method to create a new body which can be controlled by the behaviors
     * @param spawnLocation Initial location where the body should spawn
     * @param world World context in which the body should spawn
     * @param bodyType The type the body should have
     * @return Freshly created body
     */
    public static Body CreateBody(Vector2 spawnLocation, World world, BodyDef.BodyType bodyType) {
        BodyDef bDef = new BodyDef();
        bDef.position.set(spawnLocation.x / Constants.PPM, spawnLocation.y / Constants.PPM);
        bDef.type = bodyType;

        return world.createBody(bDef);
    }

    /**
     * Filter method for collision detection.
     * It will set a filter, so there is less stress on the physics engine
     * and there is less *noise* in the WorldContactListener
     * @param fixture The fixture to work on
     * @param filterBit Bit mask for the corresponding collidable objects
     */
    protected void setCategoryFilter(Fixture fixture, short filterBit) {
        Filter filter = new Filter();
        filter.categoryBits = filterBit;
        if (fixture != null) {
            fixture.setFilterData(filter);
        } else {
            // Gdx.app.debug("BEHAVIOR", "Tried to set filterdata on fixture, but fixture was null");
        }
    }

    /**
     * Takes a vector in and turns is
     * @param x Turn following the x-axis
     * @param y Turn following the y-axis
     * @param original The vector that needs updating
     */
    protected void turn(boolean x, boolean y, Vector2 original) {
        if (x)
            original.x = -original.x;
        if (y)
            original.y = -original.y;
    }
}
