package be.samclercky.heropath.behavior;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.sprites.dynamics.enemies.PigObject;
import be.samclercky.heropath.utils.LazyInit;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Timer;

import java.util.Random;

public class EnemyBehavior extends Behavior<EnemyBehavior.EnemyListener> {
    /**
     * Communication with on screen graphics
     */
    public interface EnemyListener {
        Rectangle getHitBox();
        LazyInit<Body> getBody();
    }

    /**
     * Communication with collision detection
     */
    public interface EnemyCollisionHandler {
        void onPlayerHit();
        void onWallHit();
    }

    private Vector2 velocity;

    private static final float gravityScale = 15; // Makes sure the pigs fall faster than floating

    /**
     * Constructor
     * @param interactiveObject Visual representation of the object on screen.
     * @param speed Initial speed
     * @param killableBehavior Behavior of how it wil DIE!!
     */
    public EnemyBehavior(EnemyListener interactiveObject,
                         float speed,
                         KillableBehavior killableBehavior) {
        super(interactiveObject);

        setSpeed(speed);

        Rectangle hitBox = interactiveObject.getHitBox();
        defineBody(hitBox.width, hitBox.height, killableBehavior);

        switchDirection();
    }

    /**
     * Accessor for speed
     * @param speed Initial speed
     */
    public void setSpeed(float speed) {
        Random rand = new Random();
        float sign = (rand.nextBoolean())? -1f : +1f;
        velocity = new Vector2(sign * speed, 0);
    }

    /**
     * Event handler when the EnemyObject hits a wall
     */
    private void onWallHit() {
        turn(true, false);
    }

    /**
     * Behavior for switching direction after a certain amount of time
     */
    private void switchDirection(){
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                turn(true, false);
            }
        },
        3,
        3
        );
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        interactiveObject.getBody().get().setLinearVelocity(velocity);
    }

    /**
     * Convenience method to turn the velocity
     * @param x Turn x-direction
     * @param y Turn y-direction
     */
    private void turn(boolean x, boolean y) {
        super.turn(x, y, velocity);
    }

    /**
     * Defines enemy body behavior (basic)
     */
    private void defineBody(float hitBoxWidth, float hitBoxHeight, final KillableBehavior killableBehavior) {
        FixtureDef fDef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(hitBoxWidth, hitBoxHeight);

        fDef.filter.categoryBits = Constants.ENEMY_BIT;
        fDef.filter.maskBits = Constants.OBJECT_BIT |
                Constants.PLAYER_BIT |
                Constants.PLAYER_BULLET_BIT |
                Constants.ENEMY_BIT;

        fDef.shape = shape;
        interactiveObject.getBody().get().createFixture(fDef).setUserData(new EnemyCollisionHandler() {
            @Override
            public void onPlayerHit() {
                killableBehavior.onHit(1);
                EnemyBehavior.this.onWallHit();
            }

            @Override
            public void onWallHit() {
                EnemyBehavior.this.onWallHit();
            }

        });

        interactiveObject.getBody().get().setGravityScale(gravityScale);
    }

}
