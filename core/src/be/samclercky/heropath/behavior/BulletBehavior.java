package be.samclercky.heropath.behavior;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.utils.LazyInit;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class BulletBehavior extends Behavior<BulletBehavior.BulletBehaviorListener> {

    private final boolean isBoundToGravity;
    private final KillableBehavior killableBehavior; // Needed to handle hitGround or moving out of the screen
    private final Vector2 direction;

    private final static float gravityScale = 100f;

    /**
     * Communication with on screen graphics
     */
    public interface BulletBehaviorListener {
        Rectangle getHitBox();
        LazyInit<Body> getBody();
    }

    /**
     * Communication with on screen graphics
     */
    public interface BulletCollisionHandle {
        void onHitGround();
        void onHitWall();
        void onHitPlayer();
        void onHitEnemy();
    }

    /**
     * Constructor
     * @param interactiveObject Visual representation of the object on screen.
     * @param speed Initial speed
     * @param direction Initial direction
     * @param isPlayer Is the bullet from the player?
     * @param isBouncy Is the bullet bouncy?
     * @param killableBehavior Behavior of how it wil DIE!!
     */
    public BulletBehavior(BulletBehaviorListener interactiveObject,
                          float speed,
                          Vector2 direction,
                          boolean isPlayer,
                          boolean isBouncy,
                          KillableBehavior killableBehavior) {
        super(interactiveObject);

        this.direction = new Vector2(direction.x*speed, direction.y*speed);
        this.killableBehavior = killableBehavior;
        this.isBoundToGravity = interactiveObject.getBody().get().getType() == BodyDef.BodyType.DynamicBody;

        defineBody(isPlayer, this.isBoundToGravity, isBouncy);
    }

    /**
     * Bounce logic of the bullet
     * @param vertical Is it bouncing vertical?
     * @param horizontal Is it bouncing horizontal?
     */
    private void bounce(boolean vertical, boolean horizontal) {
        turn(vertical, horizontal, direction);

        if (isBoundToGravity)
            interactiveObject.getBody().get().applyLinearImpulse(direction,
                    interactiveObject.getBody().get().getLocalCenter(),
                    false); // false ==> better perf
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        if (!isBoundToGravity)
            interactiveObject.getBody().get().setLinearVelocity(direction.x*delta, direction.y*delta);
    }

    /**
     * Defines enemy body behavior (basic)
     * @param isPlayer Is the bullet from the player?
     * @param isBoundToGravity Does the bullet have gravity?
     * @param isBouncy Is the bullet bouncy?
     */
    private void defineBody(final boolean isPlayer,
                            final boolean isBoundToGravity,
                            final boolean isBouncy) {
        FixtureDef fDef = new FixtureDef();

        CircleShape shape = new CircleShape();
        // Mean of width and height divided by 2
        shape.setRadius(
                (interactiveObject.getHitBox().width + interactiveObject.getHitBox().height)/4);

        fDef.filter.categoryBits = isPlayer ? Constants.PLAYER_BULLET_BIT : Constants.ENEMY_BULLET_BIT;
        fDef.filter.maskBits = (short) (Constants.OBJECT_BIT |
                        (isPlayer ? Constants.ENEMY_BIT : Constants.PLAYER_BIT));

        fDef.shape = shape;
        fDef.friction = 0.0f;

        // JIT optimize ==> pure functions
        interactiveObject.getBody().get().createFixture(fDef).setUserData(new BulletCollisionHandle() {
            @Override
            public void onHitGround() {
                handleBounce(true);
            }

            @Override
            public void onHitWall() {
                handleBounce(false);
            }

            private void handleBounce(boolean isGround) {
                if (isBouncy)
                    bounce(!isGround, isGround); // bounce correct direction
                else
                    killableBehavior.onHit(1000); // stop bullet
            }

            @Override
            public void onHitPlayer() {
                handleObjectHit(true);
            }

            @Override
            public void onHitEnemy() {
                handleObjectHit(false);
            }

            private void handleObjectHit(boolean isPlayerHit) {
                if (isPlayerHit ^ isPlayer) // Enemy hits Player || Player hits Enemy
                    killableBehavior.onHit(1000); // stop bullet
            }
        });

        interactiveObject.getBody().get().setGravityScale(isBoundToGravity ? gravityScale : 0);
    }
}
