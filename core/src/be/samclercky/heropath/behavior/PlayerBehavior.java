package be.samclercky.heropath.behavior;

import be.samclercky.heropath.Constants;
import be.samclercky.heropath.sprites.dynamics.PlayerObject;
import be.samclercky.heropath.utils.GamePadManager;
import be.samclercky.heropath.utils.LazyInit;
import be.samclercky.heropath.utils.XBox360Pad;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class PlayerBehavior extends Behavior<PlayerBehavior.PlayerListener> {

    /**
     * Listener for the player
     */
    public interface PlayerListener {
        void setState(PlayerObject.State state);
        World getWorld();
        Rectangle getHitBox();
        LazyInit<Body> getBody();

        void onHitWater();
        void onShoot();
        void onPlayerHit(int damage);
    }

    public interface PlayerCollisionListener {
        void onHitPlayer(int damage);
    }

    private static final float linearAccelerator = 15f / Constants.PPM;
    private static final float maxLinearSpeed = 150f / Constants.PPM;
    private static final float jumpLinearAccelerator = 400f / Constants.PPM;
    private static final float gamePadSensibility = 0.2f;

    public static final String HEAD_TAG = "player_head_tag";
    public static final String FEET_TAG = "player_feet_tag";

    private boolean isFlying = false;
    private boolean isInWater = false;

    private Fixture globalFixture;

    private final Sound gameOverSound;

    /**
     * Constructor
     * @param interactiveObject Visual representation of the object on screen.
     */
    public PlayerBehavior(PlayerListener interactiveObject) {
        super(interactiveObject);

        Rectangle hitBox = interactiveObject.getHitBox();
        defineObject(hitBox.width, hitBox.height);

        gameOverSound = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/gameOver.mp3"));
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        // respond to input
        if (Constants.ALLOW_FLIGHT) {
            DEBUGHandleInput(delta);
        } else {
            handleInput(delta);
        }

        // check movement
        checkMovement();
        checkGround();

        //check if player is in the water
        checkIfInWater();
    }

    /**
     * Convenience function to help abstract the user input
     * @param delta The time in seconds since the last render.
     */
    private void handleInput(float delta) {
        handleUp();
        handleRight();
        handleLeft();
        handleAttack();
    }

    /**
     * All possibilities for up handle
     * @return true if player should go up
     */
    private boolean isUpClicked() {
        return Gdx.input.isKeyPressed(Input.Keys.UP) ||
                Gdx.input.isKeyPressed(Input.Keys.Z) ||
                Gdx.input.isKeyPressed(Input.Keys.W) ||
                Gdx.input.isKeyPressed(Input.Keys.K) || // Vim
                GamePadManager.getAxis(XBox360Pad.AXIS_LEFT_X) < -gamePadSensibility ||
                GamePadManager.getButton(XBox360Pad.BUTTON_A);
    }

    /**
     * When the player is allowed to go up, the player goes up
     */
    private void handleUp() {
        if (
                isUpClicked() && isJumpingAllowed()
        ) {
            interactiveObject.getBody().get()
                    .applyLinearImpulse(
                            new Vector2(0, jumpLinearAccelerator),
                            interactiveObject.getBody().get().getWorldCenter(), true);
        }
    }

    /**
     * All possibilities for right handle
     * @return true if player should go up
     */
    private boolean isRightClicked() {
        return Gdx.input.isKeyPressed(Input.Keys.RIGHT) ||
                Gdx.input.isKeyPressed(Input.Keys.D) ||
                Gdx.input.isKeyPressed(Input.Keys.L) || // Vim
                GamePadManager.getAxis(XBox360Pad.AXIS_LEFT_Y) > gamePadSensibility;
    }

    /**
     * When the player is allowed to go right, the player goes right
     */
    private void handleRight() {
        if (isRightClicked() && interactiveObject.getBody().get().getLinearVelocity().x <= maxLinearSpeed) {
            // Gdx.app.debug("PlayerBehavior", "RIGHT clicked");
            interactiveObject.getBody().get()
                    .applyLinearImpulse(
                            new Vector2(linearAccelerator, 0),
                            interactiveObject.getBody().get().getWorldCenter(), true);
        }
    }

    /**
     * All possibilities for left handle
     * @return true if player should go left
     */
    private boolean isLeftClicked() {
        return Gdx.input.isKeyPressed(Input.Keys.LEFT) ||
                Gdx.input.isKeyPressed(Input.Keys.Q) ||
                Gdx.input.isKeyPressed(Input.Keys.A) ||
                Gdx.input.isKeyPressed(Input.Keys.H) || // Vim
                GamePadManager.getAxis(XBox360Pad.AXIS_LEFT_Y) < -gamePadSensibility;
    }
    /**
     * When the player is allowed to go left, the player goes left
     */
    private void handleLeft() {
        if (isLeftClicked() && interactiveObject.getBody().get().getLinearVelocity().x >= -maxLinearSpeed) {
            // Gdx.app.debug("PlayerBehavior", "LEFT clicked");
            interactiveObject.getBody().get()
                    .applyLinearImpulse(
                            new Vector2(-linearAccelerator, 0),
                            interactiveObject.getBody().get().getWorldCenter(), true);
        }
    }

    /**
     * All possibilities for attack handle
     * @return true if player should go attack
     */
    private boolean isAttackClicked() {
        return Gdx.input.isKeyJustPressed(Input.Keys.SPACE) ||
                GamePadManager.buttonJustPressed(XBox360Pad.BUTTON_X);
    }

    /**
     * When the player is allowed to attack, the player attacks
     */
    public void handleAttack() {
        if (isAttackClicked()){
            // Gdx.app.debug("AttackBehavior", "Attack key is pressed");
            interactiveObject.setState(PlayerObject.State.ATTACK);
            interactiveObject.onShoot();
        }
    }

    /**
     * Checks if the player is on the ground
     * @return true if the player is on the ground
     */
    public boolean isOnGround() {
        return interactiveObject.getBody().get().getLinearVelocity().y == 0;
    }

    /**
     * Check if the player is standing still on the ground and send status reports
     */
    private void checkGround() {
        // Check ground without movement
        if (isJumpingAllowed() && interactiveObject.getBody().get().getLinearVelocity().x == 0) {
            // Give signal to stand still
            interactiveObject.setState(PlayerObject.State.STANDING);
        } else if (!isOnGround()) { // Check in air
            interactiveObject.setState(PlayerObject.State.JUMPING);
        }
    }

    /**
     * Check for any movements of the player
     */
    private void checkMovement() {
        if (isJumpingAllowed() && interactiveObject.getBody().get().getLinearVelocity().x != 0) {
            // Give signal to run
            interactiveObject.setState(PlayerObject.State.RUNNING);
        }
    }

    /**
     * Logic when player is allowed to jump
     * @return true if the player is allowed to jump
     */
    private boolean isJumpingAllowed() {
        return interactiveObject.getBody().get().getLinearVelocity().y == 0;
    }

    /**
     * DEBUG: handle for flying
     * @return true if F handle is clicked
     */
    private boolean isFlyingRequested() {
        return Gdx.input.isKeyJustPressed(Input.Keys.F) ||
                GamePadManager.buttonJustPressed(XBox360Pad.BUTTON_Y);
    }

    /**
     * DEBUG: down handle when you are flying
     * @return true if down handle is clicked
     */
    private boolean isDownClicked() {
        return Gdx.input.isKeyPressed(Input.Keys.DOWN) ||
                Gdx.input.isKeyPressed(Input.Keys.S) ||
                Gdx.input.isKeyPressed(Input.Keys.J) || // Vim
                GamePadManager.getAxis(XBox360Pad.AXIS_LEFT_X) > gamePadSensibility;
    }

    /**
     * DEBUGGING PURPOSES ONLY
     * @param delta The time in seconds since the last render.
     */
    private void DEBUGHandleInput(float delta) {
        if (isUpClicked() && interactiveObject.getBody().get().getLinearVelocity().y <= maxLinearSpeed) {
            if (isFlying)
                interactiveObject.getBody().get().applyLinearImpulse(new Vector2(0, linearAccelerator),
                        interactiveObject.getBody().get().getWorldCenter(), true);
            else
                handleUp();
        }

        handleRight();
        handleLeft();
        handleAttack();

        if (isDownClicked() && isFlying && interactiveObject.getBody().get().getLinearVelocity().y >= -maxLinearSpeed) {
            interactiveObject.getBody().get().applyLinearImpulse(
                    new Vector2(0, -linearAccelerator),
                    interactiveObject.getBody().get().getWorldCenter(), true);
        }

        // DEBUGGING
        if (isFlyingRequested() && Constants.ALLOW_FLIGHT) {
            isFlying = !isFlying; // toggle isFlying
            // DEBUGGING ==> REMOVE GRAVITY
            if (isFlying) {
                interactiveObject.getWorld().setGravity(new Vector2(0, 0));
            } else {
                interactiveObject.getWorld().setGravity(Constants.GRAVITY);
            }
        }
    }

    /**
     * Setup of the box2D body
     */
    private void defineObject(float hitBoxWidth, float hitBoxHeight) {
        FixtureDef fdef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(hitBoxWidth, hitBoxHeight);

        fdef.filter.categoryBits = Constants.PLAYER_BIT;
        fdef.filter.maskBits = Constants.OBJECT_BIT | Constants.ENEMY_BIT | Constants.ENEMY_BULLET_BIT;

        fdef.shape = shape;
        globalFixture = interactiveObject.getBody().get().createFixture(fdef);
        globalFixture.setUserData((PlayerCollisionListener) (damage) -> {
            interactiveObject.onPlayerHit(damage);
        });
//
//        EdgeShape head = new EdgeShape();
//        head.set(new Vector2(-hitBoxWidth, hitBoxHeight + 2/Constants.PPM), new Vector2(hitBoxWidth, hitBoxHeight + 2/Constants.PPM));
//        fdef.shape = head;
//        fdef.isSensor = true;
//        interactiveObject.getBody().get().createFixture(fdef).setUserData(HEAD_TAG);

        EdgeShape feet = new EdgeShape();
        feet.set(new Vector2(-hitBoxWidth + 1/Constants.PPM, -hitBoxHeight - 2/Constants.PPM), new Vector2(hitBoxWidth - 2/Constants.PPM, -hitBoxHeight - 2/Constants.PPM));
        fdef.shape = feet;
        fdef.isSensor = true;
        fdef.filter.categoryBits = Constants.PLAYER_BIT;
        fdef.filter.maskBits = Constants.ENEMY_BIT;
        interactiveObject.getBody().get().createFixture(fdef).setUserData(FEET_TAG);

        setCategoryFilter(globalFixture, Constants.PLAYER_BIT);
    }

    /**
     * Handle if the player is in the water
     */
    private void checkIfInWater(){
        if (!isInWater && isPlayerInWater()) {
            interactiveObject.onHitWater();
            isInWater = true;
        }
    }

    /**
     * Status report if the player fell of the world
     * @return true if the player fell of the world
     */
    public boolean isPlayerInWater() {
        return interactiveObject.getBody().get() != null &&
                interactiveObject.getBody().get().getPosition().y < 0;
    }

    /**
     * logic if the player is dead
     */
    public void onPlayerDead() {
        if (!isInWater) {
            Filter filter = new Filter();
            filter.maskBits = Constants.OBJECT_BIT;

            if (globalFixture != null) {
                globalFixture.setFilterData(filter);
            }

            long idDead = gameOverSound.play();
            gameOverSound.setPitch(idDead, 1f);
            gameOverSound.setVolume(idDead, 0.3f);
            gameOverSound.setLooping(idDead, false);
        }
    }
}
