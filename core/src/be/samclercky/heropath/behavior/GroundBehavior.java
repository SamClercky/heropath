package be.samclercky.heropath.behavior;

import be.samclercky.heropath.Constants;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

/**
 * The ground should be able to collide with other entities
 * There are 2 constructors because the rectangular and arbitrary shaped groundObjects don't have a common parent
 */
public class GroundBehavior extends Behavior<GroundBehavior.GroundListener> {

    /**
     * Listener for collision with ground
     */
    public interface GroundCollisionListener {
        boolean isGround();
        void onPlayerHit();
    }

    public interface GroundListener {
        default void onPlayerHit() {
            throw new IllegalStateException("The player hit method should be implemented" +
                    " when ground is flagged as finish");
        }
    }

    /**
     * Constructor for rectangular shaped groundObjects
     * @param staticBody Box2D static body
     * @param geometry geometry data from TMX file
     * @param isGround Is there ground? yes or no.
     */
    public GroundBehavior(
            GroundListener interactiveObject,
            Body staticBody,
            Polygon geometry,
            boolean isGround,
            boolean isFinish
    ) {
        super(interactiveObject);

        defineObject(staticBody, getShapeFromPolygon(geometry), isGround, isFinish);
    }

    /**
     * Constructor for arbitrary shaped groundObjects
     * @param staticBody Box2D static body
     * @param geometry geometry data from TMX file
     * @param isGround Is there ground? yes or no.
     */
    public GroundBehavior(
            GroundListener interactiveObject,
            Body staticBody,
            Rectangle geometry,
            boolean isGround,
            boolean isFinish
    ) {
        super(interactiveObject);
        defineObject(staticBody, getShapeFromRectangle(geometry), isGround, isFinish);
    }

    /**
     * Define the Box2D ground body
     * @param body Box2D body
     * @param shape Polygonshape for body
     * @param isGround Is there ground? yes or no.
     */
    private void defineObject(Body body, PolygonShape shape, final boolean isGround, final boolean isFinish) {
        FixtureDef fDef = new FixtureDef();

        fDef.shape = shape;
        fDef.filter.categoryBits = (short) (Constants.OBJECT_BIT | (isGround ? 0 : Constants.WALL_BIT));

        Fixture fixture = body.createFixture(fDef);
        fixture.setUserData(new GroundCollisionListener() {
            @Override
            public boolean isGround() {
                return isGround;
            }

            @Override
            public void onPlayerHit() {
                if (isFinish) {
                    interactiveObject.onPlayerHit();
                }
            }
        });
    }

    /**
     * Converts polygon to PolygonShape
     * @param poly convertable polygon
     * @return shape
     */
    private PolygonShape getShapeFromPolygon(Polygon poly) {
        PolygonShape shape = new PolygonShape();
        poly.setPosition(0, 0);
        poly.setScale(1 / Constants.PPM, 1 / Constants.PPM);

        shape.set(poly.getTransformedVertices());

        return shape;
    }

    /**
     * Converts rectangle to Rectangle
     * @param rect convertable rectangle
     * @return shape
     */
    private PolygonShape getShapeFromRectangle(Rectangle rect) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(rect.getWidth()/2/ Constants.PPM,
                rect.getHeight()/2/Constants.PPM);

        return shape;
    }

    /**
     * Update method for real time rendering
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void onUpdate(float delta) {
        // Nothing to do
    }
}
