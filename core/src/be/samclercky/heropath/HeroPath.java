package be.samclercky.heropath;

import be.samclercky.heropath.screens.MenuScreen;
import be.samclercky.heropath.utils.HttpClientManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class HeroPath extends Game {
	private SpriteBatch batch;
	private AssetManager assetManager;
	private HttpClientManager httpClientManager;

	/**
	 * Accessor for the spritebatch
	 * @return SpriteBatch
	 */
	public SpriteBatch getBatch() {
		return batch;
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	public HttpClientManager getHttpClientManager() {
		return httpClientManager;
	}

	/**
	 * Manages all the assets
	 */
	private void createAssetManager(){
		assetManager = new AssetManager();
		assetManager.load("audio/music/Kyrandia-TimbermistWoods.mp3", Music.class);
		assetManager.load("audio/music/fantasy1.mp3", Music.class);
		assetManager.finishLoading();
	}

	/**
	 * Called when the Application is first created.
	 */
	@Override
	public void create () {
		batch = new SpriteBatch();

		createAssetManager();

		httpClientManager = new HttpClientManager();

		setScreen(new MenuScreen(this));
	}

	/**
	 * Called when the screen should render itself.
	 */
	@Override
	public void render () {
		super.render();
	}

	/**
	 * Called when the Application is destroyed
	 */
	@Override
	public void dispose () {
		super.dispose();
		batch.dispose();
	}
}
