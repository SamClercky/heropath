package be.samclercky.heropath;

import com.badlogic.gdx.math.Vector2;

public class Constants {
    public static final int V_WIDTH = 400; // virtual width
    public static final int V_HEIGHT = 304; // virtual height
    public static final float PPM = 100; // pixels per meter

    // World constants
    public static final Vector2 SPAWN_LOCATION = new Vector2(39500 / PPM, 30000 / PPM);
    public static final Vector2 GRAVITY = new Vector2(0, -1000 / PPM);

    // Collision filter bits
    public static final short PLAYER_BIT = 2;
    public static final short OBJECT_BIT = 4;
    public static final short WALL_BIT = 8;
    public static final short ENEMY_BIT = 16;
    public static final short PLAYER_BULLET_BIT = 32;
    public static final short ENEMY_BULLET_BIT = 64;

    // DEBUG constants
    public static final boolean ENABLE_MENU = true;
    public static final boolean ENABLE_GAME_OVER = true;
    public static final boolean ENABLE_B2DR = false; // Box2D debugger
    public static final boolean ENABLE_UI_DEBUG = false; // Lines around Table
    public static final boolean ENABLE_DEBUG_SPAWN = false; // Put initial spawn on different location
    public static final Vector2 DEBUG_SPAWN = new Vector2(17000  * 16 / PPM, 1000*16 / PPM);
    public static final boolean ALLOW_FLIGHT = false;
    public static final boolean DISABLE_HTTP_REQUESTS = false;
    public static final boolean START_IN_FULLSCREEN = false;
    public static final boolean USE_LDB = true;
    public static final boolean DISABLE_MUSIC = false; // disable music
}
